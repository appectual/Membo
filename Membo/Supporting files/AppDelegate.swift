//
//  AppDelegate.swift
//  Membo
//
//  Created by Ameya Vichare on 22/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import UserNotifications
import Firebase
import Toast_Swift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SINManagedPushDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var timer: Timer?
    let callManager = CallManager()
    weak var push: SINManagedPush?
    var friend_id = String()
    var full_name = String()
    
    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    lazy var providerDelegate: ProviderDelegate = ProviderDelegate(callManager: self.callManager)
    
    func displayIncomingCall(uuid: UUID, handle: String, hasVideo: Bool = false, completion: ((NSError?) -> Void)?) {
        providerDelegate.reportIncomingCall(uuid: uuid, handle: handle, hasVideo: hasVideo, completion: completion)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        launcher()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification),name: NSNotification.Name.MessagingRegistrationTokenRefreshed, object: nil)
        handleFCM(application)
        
        FirebaseApp.configure()
        addNumberOfTimesAppOpened()
        startLocationTimer()
        LocationService.sharedInstance.startUpdatingLocation()
        handleGoogleData()
        handleLogin()
        handleSinchNotifications(application: application)
        SinchCall.sharedInstance.establishConnection()
        
        if let id = UserDefaults.standard.value(forKey: "user_id") as? String {
            
            UserDefaults.standard.set(true, forKey: "onlineUpdated")
            SocketIOManager.sharedInstance.establishConnectionForRealTimeStatus(id: id)
        }
        else {
            
            UserDefaults.standard.set(false, forKey: "onlineUpdated")
        }

        return true
    }
    
    func addNumberOfTimesAppOpened() {
        
        if let number = UserDefaults.standard.value(forKey: "appOpened") as? Int {
            
            UserDefaults.standard.set(number + 1, forKey: "appOpened")
        }
        else {
            
            UserDefaults.standard.set(1, forKey: "appOpened")
        }
        
    }
    
    func handleSplash() {
        
        let appWindow = UIApplication.shared.keyWindow
        
        let backWhite = UIView(frame: CGRect(x: 0, y: 0, width: (self.window?.frame.size.width)!, height: (self.window?.frame.size.height)!))
        backWhite.backgroundColor = UIColor.white
        appWindow?.addSubview(backWhite)
        
        let spinner = UIImageView(frame: CGRect(x: (self.window?.frame.size.width)!/2 - 120, y: (self.window?.frame.size.height)!/2 - 120, width: 240, height: 240))
        backWhite.addSubview(spinner)
        var imgListArray = [UIImage]()
        for countValue in 1...15
        {
            
            let strImageName : String = "logo-\(countValue)"
            let image  = UIImage(named:strImageName)
            imgListArray.append(image!)
        }
        
        spinner.animationImages = imgListArray;
        spinner.animationDuration = 1.5
        spinner.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            self.handleLogin()
        }
    }
    
    func handleSinchNotifications(application: UIApplication) {
        
        print("executed handle sinch")
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        push = Sinch.managedPush(with: .production)
        push?.delegate = self
        push?.setDesiredPushTypeAutomatically()
        push?.registerUserNotificationSettings()
    }
    
    func managedPush(_ managedPush: SINManagedPush!, didReceiveIncomingPushWithPayload payload: [AnyHashable : Any]!, forType pushType: String!) {
        
        print(payload)
        print("VOICE NOTIFICATION 2")
        SinchCall.sharedInstance.sinchClient.relayRemotePushNotification(payload)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        //Sinch
        
        print("CALL NOTIFICATION")
        self.push?.application(application, didReceiveRemoteNotification: userInfo)
        
        //PN
        if application.applicationState == .active {
            //app is currently active, can update badges count here
            print("active")
            print(userInfo)
        }
        else if application.applicationState == .background {
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            print("background")
            print(userInfo)
            
        }
        else if application.applicationState == .inactive {
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            print("inactive")
        }
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
        print("messaging recieved remote message")
        print(remoteMessage.appData)
        
        if let data = remoteMessage.appData["data"] as? NSString {
            
            print(data)
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {
        print("Handle push from foreground")
 
        print(notification.request.content.userInfo)
        
        if let type = notification.request.content.userInfo["push_type"] as? String {
            
            if let friend = notification.request.content.userInfo["user_id"] as? String {
                
                full_name = notification.request.content.userInfo["full_name"] as! String
                friend_id = friend
//                handlePush(type: type)
            }
        }
        
        if let friendId = UserDefaults.standard.value(forKey: "chatIdOfOtherPerson") as? String {
            
            print("inside")
            print(friendId)
            print(friend_id)
            
            if friendId != friend_id {

                completionHandler([.alert, .badge, .sound])
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Handle push from background or closed")
        print("\(response.notification.request.content.userInfo)")

        if let type = response.notification.request.content.userInfo["push_type"] as? String {
            
            if let friend = response.notification.request.content.userInfo["user_id"] as? String {
                
                full_name = response.notification.request.content.userInfo["full_name"] as! String
                friend_id = friend
                handlePush(type: type)
            }
        }
    }
    
    func handlePush(type: String) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
        let root = storyBoard.instantiateViewController(withIdentifier: "rootNC") as! UINavigationController
        let tabBar: UIStoryboard = UIStoryboard(name:"TabBar", bundle: nil)
        let vc = tabBar.instantiateViewController(withIdentifier: "revealVC") as! SWRevealViewController
        let onboard = UIStoryboard(name:"Onboard", bundle: nil)
        let loginVC = onboard.instantiateViewController(withIdentifier: "loginVC") as! loginViewController
        
        switch type {
        case "1":
            let tab = UIStoryboard(name: "HomeTab", bundle: nil)
            if let notifvc = tab.instantiateViewController(withIdentifier: "notifVC") as? notificationViewController {
                
                root.viewControllers = [loginVC,vc]
                root.pushViewController(notifvc, animated: false)
                self.window?.rootViewController = root
                self.window?.makeKeyAndVisible()
            }
        case "2":
            let tab = UIStoryboard(name: "HomeTab", bundle: nil)
            if let locvc = tab.instantiateViewController(withIdentifier: "locReq") as? locReqViewController {
                
                root.viewControllers = [loginVC,vc]
                root.pushViewController(locvc, animated: false)
                self.window?.rootViewController = root
                self.window?.makeKeyAndVisible()
            }
        case "3":
            let tab = UIStoryboard(name: "HomeTab", bundle: nil)
            if let messageVC = tab.instantiateViewController(withIdentifier: "chatInsideVC") as? chatInsideViewController {
                
//                vc.hisImage = cell.pic.image!
                print("friendid: \(friend_id)")
                messageVC.hisName = full_name
                messageVC.friendId = friend_id
                root.viewControllers = [loginVC,vc]
                root.pushViewController(messageVC, animated: false)
                self.window?.rootViewController = root
                self.window?.makeKeyAndVisible()
            }
        case "5":
            let tab = UIStoryboard(name: "TabBar", bundle: nil)
            if let mapvc = tab.instantiateViewController(withIdentifier: "mapVC") as? mapViewController {
                
                root.viewControllers = [loginVC,vc]
                root.pushViewController(mapvc, animated: false)
                self.window?.rootViewController = root
                self.window?.makeKeyAndVisible()
            }
        default:
            break
        }
    }
    
    func startLocationTimer() {
        
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.locationUpdate), userInfo: nil, repeats: true)
    }
    
    func locationUpdate() {
        
        print("sending location")
        
        if let lat = LocationService.sharedInstance.currentLocation?.coordinate.latitude {
            
            if let long = LocationService.sharedInstance.currentLocation?.coordinate.longitude {
                
                print(lat)
                print(long)
                
                self.postServer2(param: ["userId":self.getUserId(),"weddingId":self.getWeddingId(),"latitude":lat,"longitude":long], url: K.MEMBO_ENDPOINT + "pushLocation", completion: { (json) in
                    
                    print(json)
                })
            }
        }
    }
    
    func handleLogin() {
        
        if let isLoggedIn = UserDefaults.standard.value(forKey: "isLoggedIn") as? Bool {
            
            if isLoggedIn {
                
                if let isFirstTimeLoginDone = UserDefaults.standard.value(forKey: "isFirstTimeLoginDone") as? Bool {
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
                    let root = storyBoard.instantiateViewController(withIdentifier: "rootNC") as! UINavigationController
                    
                    
                    if isFirstTimeLoginDone {
                        
                        let tabBar: UIStoryboard = UIStoryboard(name:"TabBar", bundle: nil)
                        
                        var identifier = String()
                        
                        if let admin_identifier = UserDefaults.standard.value(forKey: "admin_identifier") as? Int {
                            
                            if admin_identifier == 0 || admin_identifier == 1 {
                                
                                identifier = "revealVC"
                            }
                            else {
                                
                                identifier = "revealLogistics"
                            }
                        }
                        
                        if let vc = tabBar.instantiateViewController(withIdentifier: identifier) as? SWRevealViewController {
                            
                            root.pushViewController(vc, animated: false)
                            self.window?.makeKeyAndVisible()
                            self.window?.rootViewController = root
                        }
                    }
                    else {
                        
                        let onboard = UIStoryboard(name: "Onboard", bundle: nil)
                        if let vc = onboard.instantiateViewController(withIdentifier: "profilePicVC") as? profilePicViewController {
                            
                            root.pushViewController(vc, animated: false)
                            self.window?.makeKeyAndVisible()
                            self.window?.rootViewController = root
                        }
                    }
                }
            }
        }
    }
    
    func handleGoogleData() {
        
        GMSServices.provideAPIKey("AIzaSyDZ9IhzJEQmiJC5qNBeTyxF1GG0a_wdXVI")
        GMSPlacesClient.provideAPIKey("AIzaSyCXKkcpz3-TktVF7eW4YPzEmerNulAZW9E")
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        UserDefaults.standard.set(false, forKey: "onlineUpdated")
        UserDefaults.standard.set(true, forKey: "shouldSplash")
        timer?.invalidate()
        LocationService.sharedInstance.stopUpdatingLocation()
        SinchCall.sharedInstance.closeConnection()
        SocketIOManager.sharedInstance.closeConnection()
    }
}

extension AppDelegate {
    
    func launcher() {
        
        let whiteBack = UIView(frame: CGRect(x: 0, y: 0, width: (self.window?.frame.size.width)!, height: (self.window?.frame.size.height)!))
        whiteBack.backgroundColor = UIColor.white
//        window?.addSubview(whiteBack)
        
        let spinner = UIImageView()
        
        var imgListArray = [UIImage]()
        for countValue in 1...15
        {
            
            let strImageName : String = "logo-\(countValue)"
            let image  = UIImage(named:strImageName)
            imgListArray.append(image!)
            
            if countValue == 15 {
                
//                spinner.stopAnimating()
//                whiteBack.removeFromSuperview()
//                spinner.removeFromSuperview()
            }
        }
        
        spinner.frame = CGRect(x: 0, y: 0, width: 240, height: 240)
        spinner.animationImages = imgListArray;
        spinner.animationDuration = 3
        spinner.tag = 98
        spinner.startAnimating()
        window?.addSubview(spinner)
        
        print("launched")
    }
    
    func postServer2(param: [String: Any], url: String, completion: @escaping (JSON) -> ()) {
        
        var value = String()
        
        if let val = UserDefaults.standard.value(forKey: "user_api") as? String {
            
            value = val
        }
        let header = ["APIKEY": value]
        
        Alamofire.request(url, method: .post, parameters: param, headers: header)
            .responseJSON { response in
                
                switch response.result {
                case.success( _):
                    let json = JSON(response.result.value)
                    completion(json)
                case.failure( _):
                    
                    completion(JSON(data: Data()))
                }
        }
    }
    
    func getUserId() -> Int {
        
        if let id = UserDefaults.standard.value(forKey: "user_id") as? String {
            
            return Int(id)!
        }
        else {
            
            return 0
        }
    }
    
    func getWeddingId() -> Int {
        
        if let id = UserDefaults.standard.value(forKey: "wedding_id") as? String {
            
            return Int(id)!
        }
        else {
            
            return 0
        }
    }
}

extension AppDelegate {
    
    //////////////////////////////////
    //FCM Configuration
    //////////////////////////////////
    
    func handleFCM(_ application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.set(fcmToken, forKey: "token")
        //        sendToken(token: refreshedToken)
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
//            self.Toast(text: "Token: \(refreshedToken)")
            UserDefaults.standard.set(refreshedToken, forKey: "token")
            
            if let isTokenUploaded = UserDefaults.standard.value(forKey: "tokenUploaded") as? Bool {
                
                if !isTokenUploaded {
                    
                    //upload now
                    self.postServer2(param: ["userId": self.getUserId(), "token": refreshedToken, "platform": "ios"], url: K.MEMBO_ENDPOINT + "userToken", completion: { (json) in
                        
//                        self.Toast(text: "Uploaded token")
                        print(json)
                        
                        if json["error"].stringValue == "false" {
                            
                            
                        }
                    })
                }
            }
        }
        connectToFcm()
    }
    
    func sendToken(token: String) {
        
    }
    
    func connectToFcm() {
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print(error)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Device token: \(deviceTokenString)")
        UserDefaults.standard.set(deviceToken, forKey: "deviceTokenForSinch")
        
        self.push?.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)

        //        InstanceID.instanceID().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.unknown)
        Messaging.messaging().apnsToken = deviceToken
        
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
//            self.Toast(text: "Token: \(refreshedToken)")
            UserDefaults.standard.set(refreshedToken, forKey: "token")
        }
    }
    
    func Toast(text: String) {
        
        var style = ToastStyle()
        
        style.messageColor = UIColor.white
        
        style.messageAlignment = .center
        
        style.cornerRadius = 18
        
        style.backgroundColor = UIColor.red
        
        style.messageFont = UIFont(name: "Arial", size: 14)!
        
        ToastManager.shared.queueEnabled = false
        
        self.window?.makeToast(text, duration: 2.0, position: .center, style: style)
    }
}

