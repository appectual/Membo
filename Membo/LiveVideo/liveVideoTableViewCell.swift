//
//  liveVideoTableViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 06/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

protocol liveWatch: class {
    
    func watchLive(row: Int)
}

class liveVideoTableViewCell: UITableViewCell {
    
    weak var delegate: liveWatch?

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var liveButton: UIButton!
    
    
    @IBAction func livePressed(_ sender: UIButton) {
        
        self.delegate?.watchLive(row: sender.tag)
    }
}
