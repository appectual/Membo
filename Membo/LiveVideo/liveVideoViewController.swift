//
//  liveVideoViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SDWebImage

class liveVideoViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var liveTable: UITableView!
    
    var video = [liveVideo]()
    var liveImageHidden = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getLive()
        self.navigationController?.navigationBar.isHidden = true
        self.setupHam(navBar, button: hamButton)
        self.addSpacingToTitle(navBar, title: "LIVE VIDEO")
        self.liveTable.estimatedRowHeight = 200
        self.liveTable.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(hamOpened(_:)), name: .hamOpen, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hamClosed(_:)), name: .hamClose, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func hamOpened(_ notification: Notification) {
        
        self.view.isUserInteractionEnabled = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
    }
    
    @objc func hamClosed(_ notification: Notification) {
        
        self.view.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    @IBAction func notifPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "notifVC") as? notificationViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func chatPressed(_ sender: Any) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = homeTab.instantiateViewController(withIdentifier: "chatMainVC") as? chatMainViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getLive() {
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getLiveStreamData/\(self.getWeddingId())") { [unowned self] (json) in
            
            for item in json["liveStream"].arrayValue {
                
                self.video.append(liveVideo(ls_id: item["ls_id"].stringValue, stream_image: item["stream_image"].stringValue, title: item["title"].stringValue, unique_id: item["unique_id"].stringValue, is_live: item["is_live"].stringValue, start_date: item["start_date"].stringValue, end_date: item["end_date"].stringValue, imageHidden: false))
                
                DispatchQueue.main.async {
                    
                    self.liveTable.reloadData()
                }
            }
        }
    }
}

extension liveVideoViewController: liveWatch {
    
    func watchLive(row: Int) {
        
//        let index = IndexPath(row: row, section: 0)
//        let cell = liveTable.cellForRow(at: index) as! liveVideoTableViewCell
//        cell.img.isHidden = true
//        cell.liveButton.isHidden = true
//        video[row].imageHidden = true
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "videoEnlarged") as? videoEnlargedViewController {
            
            vc.uniqueId = video[row].unique_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension liveVideoViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return video.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "live", for: indexPath) as! liveVideoTableViewCell
            cell.delegate = self
            
            cell.img.sd_setImage(with: URL(string: video[indexPath.row].stream_image), placeholderImage: #imageLiteral(resourceName: "images"))
            cell.liveButton.layer.cornerRadius = 17
            cell.liveButton.clipsToBounds = true
            cell.liveButton.tag = indexPath.row
            
            if indexPath.row != 0 {
                
                cell.liveButton.setTitle(video[indexPath.row].title, for: .normal)
            }
            else {
                
                cell.liveButton.setTitle("GO LIVE", for: .normal)
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "event", for: indexPath) as! eventsTableViewCell
            
            cell.img.sd_setImage(with: URL(string: video[indexPath.row].stream_image), placeholderImage: #imageLiteral(resourceName: "images"))
            cell.lab.text = video[indexPath.row].title
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row != 0 {
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "videoEnlarged") as? videoEnlargedViewController {
                
                vc.uniqueId = video[indexPath.row].unique_id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            
            return 233
        }
        return 90
    }
}

extension liveVideoViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
    }
}

