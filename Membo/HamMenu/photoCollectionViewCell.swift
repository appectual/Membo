//
//  photoCollectionViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class photoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pic: UIImageView!
    @IBOutlet weak var lab: UILabel!
}
