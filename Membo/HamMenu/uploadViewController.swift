//
//  uploadViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 10/11/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import UIEmptyState
import ImagePicker

class uploadViewController: UIViewController, UIEmptyStateDelegate, UIEmptyStateDataSource, ImagePickerDelegate {
    
@IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var photoCollection: UICollectionView!
    @IBOutlet weak var navBar: UINavigationBar!
    var imageAdded = [UIImage]()
    var titlee = String()
    var isOldAlbum = Bool()
    var albumId = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.addFiller()
        uploadButton.layer.cornerRadius = 22.5
        uploadButton.clipsToBounds = true
        self.addSpacingToTitle(navBar, title: titlee)
        self.emptyStateDelegate = self
        self.emptyStateDataSource = self
        self.reloadEmptyStateForCollectionView(photoCollection)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.emptyStateDataSource = nil
        self.emptyStateDelegate = nil
    }
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "images")
    }
    
    var emptyStateTitle: NSAttributedString {
        
        let attrs = [NSForegroundColorAttributeName: UIColorFromHex(rgbValue: 0x000000),
                     NSFontAttributeName: UIFont.systemFont(ofSize: 15)]
        return NSAttributedString(string: "ADD IMAGES", attributes: attrs)
    }
    
    var emptyStateDetailMessage: NSAttributedString? {
        
        let attrs = [NSForegroundColorAttributeName: UIColor.black,
                     NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
        return NSAttributedString(string: "Click on the + icon in the top right corner to add images", attributes: attrs)
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addPressed(_ sender: Any) {
        
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        dismiss(animated: true, completion: nil)
        
        let newImageArray = imageAdded + images
        imageAdded = newImageArray
        
        DispatchQueue.main.async {
            
            self.reloadEmptyStateForCollectionView(self.photoCollection)
            self.photoCollection.reloadData()
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        
        
    }
    
    @IBAction func uploadPressed(_ sender: Any) {
        
        if imageAdded.count > 0 {
            
            if isOldAlbum {
                
                self.spinnerStart()
                
                for i in 0..<self.imageAdded.count {
                    
                    if let base64String = UIImageJPEGRepresentation(self.imageAdded[i], 0.6)?.base64EncodedString() {
                        
                        if i != self.imageAdded.count - 1 {
                            
                            self.uploadAfterFirstImage(param: ["userId":self.getUserId(), "album_name":self.titlee, "wedding_id":self.getWeddingId(), "album_id": albumId, "count":1, "base64String": base64String],isLast: false)
                        }
                        else {
                            
                            self.uploadAfterFirstImage(param: ["userId":self.getUserId(), "album_name":self.titlee, "wedding_id":self.getWeddingId(), "album_id": albumId, "count":1, "base64String": base64String],isLast: true)
                        }
                    }
                }
            }
            else {
                
                var param = [String:Any]()
                
                if imageAdded.indices.contains(0) {
                    
                    self.spinnerStart()
                    
                    if let base64String = UIImageJPEGRepresentation(imageAdded[0], 0.6)?.base64EncodedString() {
                        
                        param = ["userId":self.getUserId(), "album_name":titlee, "wedding_id":self.getWeddingId(), "album_id":0, "count":0, "base64String": base64String]
                    }
                    
                    self.postServer2(param: param, url: K.MEMBO_ENDPOINT + "uploadGalleryPicture") { [unowned self] (json) in
                        
                        print(json)
                        
                        if json["error"].stringValue == "false" {
                            
                            if self.imageAdded.count > 1 {
                                
                                for i in 1..<self.imageAdded.count {
                                    
                                    if let base64String = UIImageJPEGRepresentation(self.imageAdded[i], 0.6)?.base64EncodedString() {
                                        
                                        if i != self.imageAdded.count - 1 {
                                            
                                            self.uploadAfterFirstImage(param: ["userId":self.getUserId(), "album_name":self.titlee, "wedding_id":self.getWeddingId(), "album_id": json["post_count"].stringValue, "count":1, "base64String": base64String],isLast: false)
                                        }
                                        else {
                                            
                                            self.uploadAfterFirstImage(param: ["userId":self.getUserId(), "album_name":self.titlee, "wedding_id":self.getWeddingId(), "album_id": json["post_count"].stringValue, "count":1, "base64String": base64String],isLast: true)
                                        }
                                    }
                                }
                            }
                            else {
                                
                                self.stopSpinner()
                                self.Toast(text: "Album uploaded.")
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                        else {
                            
                            self.stopSpinner()
                            self.Toast(text: "Something went wrong. Try again later.")
                        }
                    }
                }
            }
        }
        else {
            
            self.Toast(text: "Add some images to upload.")
        }
    }
    
    func uploadAfterFirstImage(param: [String:Any], isLast: Bool) {
        
        self.postServer2(param: param, url: K.MEMBO_ENDPOINT + "uploadGalleryPicture") { [unowned self] (json) in
            
            print(json)
            
            if json["error"].stringValue == "false" {
                
                if isLast {
                    
                    self.stopSpinner()
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else {
                
                self.Toast(text: "Something went wrong. Try again later.")
            }
        }
    }
}

extension uploadViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imageAdded.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "img", for: indexPath) as! photoCollectionViewCell
        cell.pic.image = imageAdded[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let constant = self.view.frame.size.width * 107.8333/325
        
        return CGSize(width: constant, height: constant)
    }
}
