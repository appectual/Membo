//
//  blessingsViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class blessingsViewController: UIViewController {

    @IBOutlet weak var blessingTable: UITableView!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var floatingButton: UIButton!
    
    var blessing = [Blessing]()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        getBlessings()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.blessingTable.addSubview(refreshControl)
        self.addSpacingToTitle(navBar, title: "BLESSINGS")
        setupTable()
        makeItRound(floatingButton, radius: 24, varyingWidth: false)
        setUpHamburger()
        getBlessings()
        
        let revealController: SWRevealViewController? = revealViewController()
        revealViewController().delegate = self
    }
    
    func setUpHamburger() {
        
        let revealController: SWRevealViewController? = revealViewController()
        _ = revealController?.panGestureRecognizer()
        _ = revealController?.tapGestureRecognizer()
        
        hamButton.target = revealViewController()
        hamButton.action = #selector(SWRevealViewController.revealToggle(_:))
    }
    
    func getBlessings() {
        
        blessing.removeAll()
        DispatchQueue.main.async {
            
            self.blessingTable.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getBlessing/\(self.getWeddingId())") { [unowned self] (json) in
            
            self.refreshControl.endRefreshing()
            for item in json["blessing"].arrayValue {

                self.blessing.append(Blessing(username: item["username"].stringValue, message: item["message"].stringValue, blessingsText: item["blessingsText"].stringValue, timeElapsed: item["timeElapsed"].stringValue, profilePic: item["profilePic"].stringValue, blessingsTemplate: item["blessingsTemplate"].stringValue))
                
                DispatchQueue.main.async {
                    
                    self.blessingTable.reloadData()
                }
            }
            
            if json["error"].stringValue == "true" {
                
                self.Toast(text: "No blessings found")
            }
        }
    }
    
    func setupTable() {
        
        blessingTable.tableFooterView = UIView(frame: CGRect.zero)
        blessingTable.estimatedRowHeight = 40
        blessingTable.rowHeight = UITableViewAutomaticDimension
    }
    
    @IBAction func floatingPressed(_ sender: Any) {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "addBlessingVC") as? addBlessingViewController {
            
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension blessingsViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
        }
    }
}

extension blessingsViewController: blessingReload {
    
    func reloadIt() {
        
        getBlessings()
    }
}

extension blessingsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return blessing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var bless: Blessing
        bless = blessing[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "blessing", for: indexPath) as! feedTableViewCell
        
        makeItRound(cell.profilePic, radius: 22.5, varyingWidth: true)
        cell.profilePic.sd_setImage(with: URL(string: bless.profilePic), placeholderImage: #imageLiteral(resourceName: "images"))
        cell.title.text = bless.username.capitalized
        cell.time.text = bless.timeElapsed
        cell.message.text = bless.message
        cell.feedPic.sd_setImage(with: URL(string: bless.blessingsTemplate), placeholderImage: #imageLiteral(resourceName: "images"))
        
        cell.selectionStyle = .none
        return cell
    }
    
    func makeItRound(_ pic: UIView, radius: CGFloat, varyingWidth: Bool) {
        
        if varyingWidth {
            
            pic.layer.cornerRadius = self.view.frame.size.width * radius/414
        }
        else {
            
            pic.layer.cornerRadius = radius
        }
        pic.clipsToBounds = true
    }
}
