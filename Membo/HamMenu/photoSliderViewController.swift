//
//  photoSliderViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 10/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SDWebImage

class photoSliderViewController: UIViewController {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var photoCollection: UICollectionView!
    var photoArray = [Album]()
    var index = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(index)
        let indexPath = IndexPath(row: index, section: 0)
        
        self.photoCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        self.addSpacingToTitle(navBar, title: "PHOTOS")
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

extension photoSliderViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pic", for: indexPath) as! photoCollectionViewCell
        
        cell.pic.sd_setImage(with: URL(string: photoArray[indexPath.row].album_image), placeholderImage: #imageLiteral(resourceName: "images"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.size.width, height: 300)
    }
}
