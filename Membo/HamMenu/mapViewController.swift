//
//  mapViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage

class mapViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var cross: UIBarButtonItem!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var profilepic: UIImageView!
    @IBOutlet weak var nameDetail: UILabel!
    @IBOutlet weak var mobileDetail: UILabel!
    @IBOutlet weak var overlay: UIView!
    @IBOutlet weak var locationButton: UIButton!
    
    let locationManager = CLLocationManager()
    var loc = CLLocationCoordinate2D()
    var gotLocation = Bool()
    var mapView = GMSMapView()
    @IBOutlet weak var searchBar: leftPaddedTextField!
    var isSearching = Bool()
    @IBOutlet weak var userTable: UITableView!
    var userMap = [User]()
    var filteredUser = [User]()
    var mapMarkers = [GMSMarker]()
    var isAdm = Bool()
    var currentMarker = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("loaded")
        self.hideKeyboardWhenTappedAround()
        self.addSpacingToTitle(navBar, title: "MAP")
        initialSetup()
        askForLocationPermission()
        setupSearch(searchBar)
        
        if self.isAdmin() == 0 {
            
            getTrackingData()
        }
        else if self.isAdmin() == 1 || self.isAdmin() == 3 {
            
            getAdminTrackingData()
        }
        
        if isAdm {
            
            hamButton.image = #imageLiteral(resourceName: "back")
        }
        else {
            
            self.setupHam(navBar, button: hamButton)
            let revealController: SWRevealViewController? = revealViewController()
            revealViewController().delegate = self
        }

        cross.image = nil
        cross.isEnabled = false
        
        makeItRound(profilepic, radius: 20)
        makeItRound(chatButton, radius: 20)
        makeItRound(callButton, radius: 20)
        makeItRound(locationButton, radius: 20)
        
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    func makeItRound(_ view: UIView, radius: Int) {
        
        view.layer.cornerRadius = CGFloat(radius)
        view.clipsToBounds = true
    }
    
    @IBAction func callPressed(_ sender: Any) {
        
        if isSearching {
            
            if profilepic.image != nil {
                
                SinchCall.sharedInstance.callUser(filteredUser[currentMarker].id, name: filteredUser[currentMarker].userName, profile: profilepic.image!)
            }
            else {
                
                SinchCall.sharedInstance.callUser(filteredUser[currentMarker].id, name: filteredUser[currentMarker].userName, profile: #imageLiteral(resourceName: "images"))
            }
            
        }
        else {
            
            if profilepic.image != nil {
                
                SinchCall.sharedInstance.callUser(userMap[currentMarker].id, name: userMap[currentMarker].userName, profile: profilepic.image!)
            }
            else {
                
                SinchCall.sharedInstance.callUser(userMap[currentMarker].id, name: userMap[currentMarker].userName, profile: #imageLiteral(resourceName: "images"))
            }
        }
    }
    
    @IBAction func locationPressed(_ sender: Any) {
        
        var lat = String()
        var long = String()
        
        if isSearching {
            
            lat = filteredUser[currentMarker].lat
            long = filteredUser[currentMarker].long
        }
        else {
            
            lat = userMap[currentMarker].lat
            long = userMap[currentMarker].long
        }
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(NSURL(string:
                    "comgooglemaps://?saddr=&daddr=\(lat),\(long)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
            
        } else {
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL)) {
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(NSURL(string:
                        "https://maps.google.com/?saddr=&daddr=\(lat),\(long)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    @IBAction func chatPressed(_ sender: Any) {
        
        let tab = UIStoryboard(name: "HomeTab", bundle: nil)
        if let vc = tab.instantiateViewController(withIdentifier: "chatInsideVC") as? chatInsideViewController {
            
            if isSearching {
                
                vc.hisName = filteredUser[currentMarker].userName
                vc.friendId = filteredUser[currentMarker].id
            }
            else {
                
                vc.hisName = userMap[currentMarker].userName
                vc.friendId = userMap[currentMarker].id
            }

            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func closeDetailPressed(_ sender: Any) {
        
        overlay.isHidden = true
        detailView.isHidden = true
    }
    
    @IBAction func crossPressed(_ sender: Any) {
        
        cross.image = nil
        cross.isEnabled = false
        
        for i in 0..<userMap.count {
            
            let location = CLLocation(latitude: Double(userMap[i].lat)!, longitude: Double(userMap[i].long)!)
            self.addMarker(location: location, name: userMap[i].userName, index: i)
        }
        
        let position = CLLocationCoordinate2DMake(loc.latitude,loc.longitude)
        let marker = GMSMarker(position: position)
        marker.map = self.mapView
        
        moveMap(location: LocationService.sharedInstance.currentLocation!)
    }
    
    @IBAction func hamPressed(_ sender: Any) {
        
        if isAdm {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func setupSearch(_ textField: UITextField) {
        
        textField.layer.cornerRadius = 17.5
        textField.clipsToBounds = true
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColorFromHex(rgbValue: 0x999999).cgColor
        textField.delegate = self
        textField.addTarget(self, action: #selector(self.textfieldChanged(_:)), for: .editingChanged)
        
        let searchImage = UIImageView(frame: CGRect(x: 13, y: 9, width: 17, height: 17))
        searchImage.image = #imageLiteral(resourceName: "searchBar")
        textField.addSubview(searchImage)
    }
    
    func initialSetup() {
        
        locationManager.delegate = self
//        mapView.delegate = self
    }
    
    func getAdminTrackingData() {
        
        var i = 0
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getAdminLocationTrackingData/\(self.getUserId())/\(self.getWeddingId())") { [unowned self] (json) in
            
            print(json)
            
            if json["error"].stringValue == "false" {
                
                for item in json["requests"].arrayValue {
                    
                    self.userMap.append(User(lat: item["location"]["latitude"].stringValue, long: item["location"]["longitude"].stringValue, userName: item["userName"].stringValue, id: item["userId"].stringValue,mobile: item["mobileNo"].stringValue, pic: item["profilePic"].stringValue))
                    let location = CLLocation(latitude: Double(item["location"]["latitude"].stringValue)!, longitude: Double(item["location"]["longitude"].stringValue)!)
                    self.addMarker(location: location, name: item["userName"].stringValue, index: i)
                    i += 1
                }
            }
        }
    }
    
    func getTrackingData() {
        
        var i = 0
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getLocationTrackingData/\(self.getUserId())/\(self.getWeddingId())") { [unowned self] (json) in
            
            print(json)
            
            if json["error"].stringValue == "false" {
                
                for item in json["requests"].arrayValue {
                    
                    self.userMap.append(User(lat: item["location"]["latitude"].stringValue, long: item["location"]["longitude"].stringValue, userName: item["userName"].stringValue, id: item["userId"].stringValue,mobile: item["mobileNo"].stringValue, pic: item["profilePic"].stringValue))
                    let location = CLLocation(latitude: Double(item["location"]["latitude"].stringValue)!, longitude: Double(item["location"]["longitude"].stringValue)!)
                    self.addMarker(location: location, name: item["userName"].stringValue, index: i)
                    i += 1
                }
            }
        }
    }
    
    func searchQuery(query: String) {
        
        let searchString = query
        
        filteredUser = userMap.filter({ (user) -> Bool in
            let username: NSString = user.userName as NSString
            
            return (username.range(of: searchString, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        
        DispatchQueue.main.async {
            
            self.userTable.reloadData()
        }
    }
}

extension mapViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
        }
    }
}

extension mapViewController: UITextFieldDelegate {
    
    func textfieldChanged(_ textField: UITextField) {
        
        if (textField.text?.characters.count)! > 0 {
            
            print("hello")
            isSearching = true
            userTable.isHidden = false
            self.view.bringSubview(toFront: userTable)
            //show table
            
            searchQuery(query: textField.text!)
        }
        else {
            
            isSearching = false
            userTable.isHidden = true
            //hide table
            DispatchQueue.main.async {

                self.userTable.reloadData()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        searchBar.resignFirstResponder()
        isSearching = false
        userTable.isHidden = true
        
        return true
    }
}

extension mapViewController: CLLocationManagerDelegate, GMSMapViewDelegate, UIScrollViewDelegate {
    
    func askForLocationPermission() {
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch(CLLocationManager.authorizationStatus()) {
                
            case .notDetermined, .restricted, .denied:
                locationManager.requestWhenInUseAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.startUpdatingLocation()
            }
        }
        else {
            print("Location services are not enabled")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
        if let location = locations.first {
            if !gotLocation {
                changeMapPosition(location: location)
                moveMap(location: location)
                locationManager.stopUpdatingLocation()
                gotLocation = true
            }
        }
    }
    
    func changeMapPosition(location: CLLocation) {
        
        loc = location.coordinate
        let camera = GMSCameraPosition.camera(withLatitude: loc.latitude, longitude: loc.longitude, zoom: 12)
        let frame = CGRect(x: 0, y: 64 + 62, width: self.view.frame.size.width, height: self.view.frame.size.height - 64 - 62)
        mapView = GMSMapView.map(withFrame: frame, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        self.view.addSubview(mapView)
        
        let position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.map = mapView
        
//        userMap.append(User(lat: "\(location.coordinate.latitude)", long: "\(location.coordinate.longitude)", userName: self.getUsername()))
    }
    
    func addMarker(location: CLLocation, name: String, index: Int) {
        
        let width = self.box(text: name, fontSize: 12, fontName: "OpenSans-Regular").0
        let height = self.box(text: name, fontSize: 12, fontName: "OpenSans-Regular").1
        
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: width + 16, height: height + 16 + 8))
        
        let bigView = UIView(frame: CGRect(x: 0, y: 0, width: width + 16, height: height + 16))
        bigView.backgroundColor = UIColor(red: 41.0/255.0, green: 128.0/255.0, blue: 185.0/255.0, alpha: 1.0)
        bigView.layer.cornerRadius = 8
        bigView.clipsToBounds = true
        
        mainView.addSubview(bigView)
        
        let lab = UILabel(frame: CGRect(x: 5, y: 5, width: width + 6, height: height + 6))
        lab.text = name
        lab.textColor = UIColor.white
        lab.font = UIFont(name: "OpenSans-Regular", size: 12)
        lab.textAlignment = .center
        bigView.addSubview(lab)
        
        let triangle = TriangleView(frame: CGRect(x: (width + 16)/2 - 6, y: height + 16, width: 12, height: 8))
        triangle.backgroundColor = UIColor.clear
        triangle.layer.setAffineTransform(CGAffineTransform(scaleX: 1, y: -1))
        mainView.addSubview(triangle)
        
        let position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.iconView = mainView
        marker.map = mapView
        marker.userData = index
        
        mapMarkers.append(marker)
    }
    
    func moveMap(location: CLLocation) {

        let fancy = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 12)
        self.mapView.animate(to: fancy)
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        
        moveMap(location: LocationService.sharedInstance.currentLocation!)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {

        if let marker_id = marker.userData as? Int {
            
            if isSearching {
                
                if filteredUser[marker_id].id != "\(self.getUserId())" {
                    
                    overlay.isHidden = false
                    detailView.isHidden = false
                    self.view.bringSubview(toFront: overlay)
                    self.view.bringSubview(toFront: detailView)
                    currentMarker = marker_id
                    
                    nameDetail.text = filteredUser[marker_id].userName
                    mobileDetail.text = filteredUser[marker_id].mobile
                    profilepic.sd_setImage(with: URL(string: filteredUser[marker_id].pic), placeholderImage: #imageLiteral(resourceName: "images"))
                }
            }
            else {
                
                if userMap[marker_id].id != "\(self.getUserId())" {
                    
                    overlay.isHidden = false
                    detailView.isHidden = false
                    self.view.bringSubview(toFront: overlay)
                    self.view.bringSubview(toFront: detailView)
                    currentMarker = marker_id
                    
                    nameDetail.text = userMap[marker_id].userName
                    mobileDetail.text = userMap[marker_id].mobile
                    profilepic.sd_setImage(with: URL(string: userMap[marker_id].pic), placeholderImage: #imageLiteral(resourceName: "images"))
                }
            }
        }
        
        return true
    }
}

extension mapViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !isSearching {
            
            return userMap.count
        }
        else {
            
            return filteredUser.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "user", for: indexPath)
        
        if isSearching {
            
            cell.textLabel?.text = filteredUser[indexPath.row].userName
        }
        else {
            
            cell.textLabel?.text = userMap[indexPath.row].userName
        }

        cell.textLabel?.font = UIFont(name: "OpenSans-Regular", size: 16.0)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !isSearching {
            
            
        }
        else {
            cross.isEnabled = true
            cross.image = #imageLiteral(resourceName: "cross")
            userTable.isHidden = true
            searchBar.text?.removeAll()
            mapView.clear()
            
            let position = CLLocationCoordinate2DMake(loc.latitude,loc.longitude)
            let marker = GMSMarker(position: position)
            marker.map = self.mapView
            
            let location = CLLocation(latitude: Double(filteredUser[indexPath.row].lat)!, longitude: Double(filteredUser[indexPath.row].long)!)
            self.addMarker(location: location, name: filteredUser[indexPath.row].userName, index: 0)
            moveMap(location: location)
        }
    }
}
