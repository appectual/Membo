//
//  albumViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SDWebImage

class albumViewController: UIViewController {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var hamButton: UIBarButtonItem!
    @IBOutlet weak var albumCollection: UICollectionView!
    @IBOutlet weak var newAlbumButton: UIButton!
    
    var album = [Album]()
    var isAdm = Bool()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        getAlbums()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        self.albumCollection.addSubview(refreshControl)
        self.addSpacingToTitle(navBar, title: "ALBUMS")
        
        if isAdm {
            
            hamButton.image = #imageLiteral(resourceName: "back")
        }
        else {
            
            setUpHamburger()
        }
        
        let revealController: SWRevealViewController? = revealViewController()
        revealViewController().delegate = self
        
        if self.isAdmin() == 1 {
            
            newAlbumButton.layer.cornerRadius = 24
            newAlbumButton.clipsToBounds = true
            self.newAlbumButton.isHidden = false
        }
        else {

            self.newAlbumButton.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getAlbums()
    }
    
    @IBAction func addAlbumPressed(_ sender: Any) {
        
        if let popOverVC = storyboard?.instantiateViewController(withIdentifier: "albumPopUp") as? albumPopViewController {
            
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            self.didMove(toParentViewController: self)
        }
    }
    
    @IBAction func hamPressed(_ sender: Any) {
        
        if isAdm {
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func getAlbums() {
        
        album.removeAll()
        
        DispatchQueue.main.async {
            
            self.albumCollection.reloadData()
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getAlbums/\(self.getWeddingId())") { [unowned self] (json) in
            
            self.refreshControl.endRefreshing()
            
            for item in json["album"].arrayValue {
                    
                self.album.append(Album(album_id: item["album_id"].stringValue, admin_id: item["admin_id"].stringValue, album_name: item["album_name"].stringValue, album_image: item["album_image"].stringValue))
                
                DispatchQueue.main.async {
                    
                    self.albumCollection.reloadData()
                }
            }
            
            if json["error"].stringValue == "true" {
                
                self.Toast(text: "No albums found")
            }
        }
    }
    
    func setUpHamburger() {
        
        let revealController: SWRevealViewController? = revealViewController()
        _ = revealController?.panGestureRecognizer()
        _ = revealController?.tapGestureRecognizer()
        
        hamButton.target = revealViewController()
        hamButton.action = #selector(SWRevealViewController.revealToggle(_:))
    }
}

extension albumViewController: SWRevealViewControllerDelegate {
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
        if position == .left {
            
            self.view.isUserInteractionEnabled = true
        }
        else {
            
            self.view.isUserInteractionEnabled = false
        }
    }
}

extension albumViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return album.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var alb: Album
        alb = album[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pic", for: indexPath) as! photoCollectionViewCell
        
        cell.pic.sd_setImage(with: URL(string: alb.album_image), placeholderImage: #imageLiteral(resourceName: "images"))
        cell.lab.text = alb.album_name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.view.frame.size.width
        return CGSize(width: width/2, height: width/2)
    }
}

extension albumViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var alb: Album
        alb = album[indexPath.row]
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "albumDetailVC") as? albumDetailViewController {
            
            vc.detail = alb.album_name
            vc.albumId = alb.album_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
