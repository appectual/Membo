//
//  addBlessingViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SDWebImage

protocol blessingReload {
    
    func reloadIt()
}

class addBlessingViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var placeholder: UILabel!
    @IBOutlet weak var addText: UITextView!
    @IBOutlet weak var blessingCollection: UICollectionView!
    
    var delegate: blessingReload?
    var selectedTemp = 0
    var blessingTemp = [BlessingTemplate]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        makeItRound(postButton, radius: 20.5)
        self.addSpacingToTitle(navBar, title: "ADD BLESSINGS")
        addText.textContainerInset = UIEdgeInsets(top: 12, left: 9, bottom: 12, right: 9)
        getBlessingTemplate()
    }
    
    func getBlessingTemplate() {
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getBlessingTemplates/\(self.getWeddingId())") { [unowned self] (json) in
            
            print(json)
            
            for item in json["blessing_template"].arrayValue {
                    
                self.blessingTemp.append(BlessingTemplate(blessingsId: item["blessingsId"].stringValue, blessingsTemplate: item["blessingsTemplate"].stringValue))
                
                DispatchQueue.main.async {
                    
                    self.blessingCollection.reloadData()
                }
            }
        }
    }
    
    @IBAction func postPressed(_ sender: Any) {

        if (addText.text.characters.count) > 0 {
                
            self.postServer(param: ["user_id": self.getUserId(), "blessing_id": blessingTemp[selectedTemp].blessingsId, "message": addText.text], url: K.MEMBO_ENDPOINT + "postBlessing") { [unowned self] (json) in
                
                if json["error"].stringValue == "false" {
                    
                    self.Toast(text: "Your blessing is posted.")
                    self.delegate?.reloadIt()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                else if json["error"].stringValue == "true" {
                    
                    self.Toast(text: "Something went wrong. Try again later.")
                }
            }
        }
        else {
            
            self.Toast(text: "Add a message.")
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    func makeItRound(_ pic: UIView, radius: CGFloat) {
        
        pic.layer.cornerRadius = self.view.frame.size.width * radius/414
        pic.clipsToBounds = true
    }
}

extension addBlessingViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        checkIfTextIsEmpty(textView)
    }

    func checkIfTextIsEmpty(_ textView: UITextView) {
        
        if textView.text.characters.count > 0 {
            
            placeholder.isHidden = true
        }
        else {
            
            placeholder.isHidden = false
        }
    }
}

extension addBlessingViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        collectionHeight.constant = CGFloat(116 * blessingTemp.count/2)
        return blessingTemp.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var temp : BlessingTemplate
        temp = blessingTemp[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bless", for: indexPath) as! photoCollectionViewCell
        cell.pic.sd_setImage(with: URL(string: temp.blessingsTemplate), placeholderImage: #imageLiteral(resourceName: "images"))
        
        if selectedTemp == indexPath.row {
            
            cell.pic.layer.borderColor = UIColor.red.cgColor
            cell.pic.layer.borderWidth = 5
        }
        else {
            
            cell.pic.layer.borderColor = UIColor.clear.cgColor
            cell.pic.layer.borderWidth = 0
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = self.view.frame.size.width
        return CGSize(width: width/2, height: 116)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedTemp = indexPath.row
        
        DispatchQueue.main.async {
            
            self.blessingCollection.reloadData()
        }
    }
}
