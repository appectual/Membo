//
//  accomodationsViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 26/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SDWebImage
import UIEmptyState

class accomodationsViewController: UIViewController, UIEmptyStateDelegate, UIEmptyStateDataSource {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var detailTable: UITableView!
    @IBOutlet weak var directionButton: UIButton!
    
    var detailArray = [String]()
    var lat = String()
    var long = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        detailTable.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80))
        self.addSpacingToTitle(navBar, title: "ACCOMODATIONS")
        addHeader()
        makeItRound(directionButton)
        getDetails()
        self.emptyStateDelegate = self
        self.emptyStateDataSource = self
    }
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "hotelBig")
    }
    
    var emptyStateTitle: NSAttributedString {
        
        let attrs = [NSForegroundColorAttributeName: UIColor.black,
                     NSFontAttributeName: UIFont(name: "OpenSans-SemiBold", size: 20)]
        return NSAttributedString(string: "No Accomodation assigned.", attributes: attrs)
    }
    
    var emptyStateDetailMessage: NSAttributedString? {
        
        let attrs = [NSForegroundColorAttributeName: UIColorFromHex(rgbValue: 0x333333),
                     NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 16)]
        
            
        return NSAttributedString(string: "Looks like you haven't been asigned an accomodation yet.", attributes: attrs)
    }
    
    func getDetails() {
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getMyAccommodation/\(self.getUserId())/\(self.getWeddingId())") { [unowned self] (json) in
            
            if json["error"].stringValue == "false" {
                
                self.detailArray.append(json["userAccommodation"]["roomNumber"].stringValue)
                self.detailArray.append(json["userAccommodation"]["venueName"].stringValue)
                self.detailArray.append(json["userAccommodation"]["venueAddress"].stringValue)
                self.detailArray.append(json["userAccommodation"]["venueAddress"].stringValue)
                self.lat = json["userAccommodation"]["venueLat"].stringValue
                self.long = json["userAccommodation"]["venueLong"].stringValue
                
                DispatchQueue.main.async {
                    
                    self.directionButton.isHidden = false
                    self.detailTable.reloadData()
                    self.reloadEmptyStateForTableView(self.detailTable)
                }
            }
            else {
                
                DispatchQueue.main.async {
                    
                    self.directionButton.isHidden = true
                    self.detailTable.reloadData()
                    self.reloadEmptyStateForTableView(self.detailTable)
                }
            }
        }
    }
    
    func makeItRound(_ button: UIButton) {
        
        button.layer.cornerRadius = 15
        button.clipsToBounds = true
        button.addTextSpacing(spacing: 1.1)
    }
    
    func addHeader() {
        
        let lab = UILabel(frame: CGRect(x: 18, y: 0, width: self.view.frame.size.width - 36, height: 90))
        lab.text = "We wish you enjoy the stay and create memories with Membo. Thank you!"
        lab.font = UIFont(name: "OpenSans-SemiBold", size: 13.8)
        lab.numberOfLines = 0
        lab.textAlignment = .center
        lab.textColor = UIColorFromHex(rgbValue: 0x999999)
        detailTable.tableHeaderView = lab
        detailTable.estimatedRowHeight = 40
        detailTable.rowHeight = UITableViewAutomaticDimension
        detailTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func directionPressed(_ sender: Any) {
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            
            UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(self.lat),\(self.long)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
            
        } else {
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL)) {
                
                UIApplication.shared.open(NSURL(string:
                    "https://maps.google.com/?saddr=&daddr=\(self.lat),\(self.long)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
            }
        }
    }
}

extension accomodationsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return detailArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 3 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "map", for: indexPath) as! mapInAccomodationsTableViewCell
            let mapsUrl = "http://maps.google.com/maps/api/staticmap?center=\(lat),%20\(long)&zoom=18&size=414x160&sensor=false&markers=color:red%7C\(lat),%20\(long)"
            cell.map.sd_setImage(with: URL(string: mapsUrl), placeholderImage: #imageLiteral(resourceName: "images"))
            cell.selectionStyle = .none
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! chatMainTableViewCell
            
            cell.name.addTextSpacing(spacing: 1.08)
            cell.location.addTextSpacing(spacing: 1.08)
            cell.location.text = detailArray[indexPath.row]
            
            switch indexPath.row {
            case 0:
                cell.name.text = "Room No."
                cell.pic.image = #imageLiteral(resourceName: "key")
            case 1:
                cell.name.text = "Venue Name"
                cell.pic.image = #imageLiteral(resourceName: "hotelBig")
            case 2:
                cell.name.text = "Venue Address"
                cell.pic.image = #imageLiteral(resourceName: "hotelBig")
            default:
                break
            }
            
            cell.selectionStyle = .none
            return cell
        }
    }
}
