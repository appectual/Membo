//
//  albumPopViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 10/11/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class albumPopViewController: UIViewController {

    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var albumName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        showAnimation()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    func showAnimation() {
        
        //zoom in animation for popup
        self.popView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.popView.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.popView.alpha = 1
            self.popView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        
        //zoom out for closing view
        self.popView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.popView.alpha = 1
        UIView.animate(withDuration: 0.3, animations: {
            self.popView.alpha = 0
            self.popView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }) { (result) in
            self.view.removeFromSuperview()
        }
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        
        removeAnimation()
    }
    
    @IBAction func okPressed(_ sender: Any) {
        
        //add album & refresh
        
        if (albumName.text?.characters.count)! > 0 {
            
            removeAnimation()
            
            if let vc = storyboard?.instantiateViewController(withIdentifier: "uploadVC") as? uploadViewController {
                
                vc.titlee = albumName.text!
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else {
            
            self.Toast(text: "Album name cannot be empty.")
        }

//        
    }
}

extension albumPopViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
}
