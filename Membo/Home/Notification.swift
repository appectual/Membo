//
//  Notification.swift
//  Membo
//
//  Created by Ameya Vichare on 06/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class NotificationMembo {
    
    var notification_id: String
    var user_id: String
    var sender_id: String
    var notification_type: String
    var description: String
    var activity: String
    var date_time: String
    
    init(notification_id: String, user_id: String, sender_id: String, notification_type: String, description: String, activity: String, date_time: String) {
        
        self.notification_id = notification_id
        self.user_id = user_id
        self.sender_id = sender_id
        self.notification_type = notification_type
        self.description = description
        self.activity = activity
        self.date_time = date_time
    }
}
