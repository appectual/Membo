//
//  locReqTableViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 08/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

protocol locationAccess: class {
    
    func alwaysSelected(section: Int)
    func onlySelected(section: Int)
    func accepted(section: Int)
    func declined(section: Int)
}

class locReqTableViewCell: UITableViewCell {
    
    weak var delegate: locationAccess?

    @IBOutlet weak var pic: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var onlyButton: UIButton!
    @IBOutlet weak var alwaysButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    
    @IBAction func acceptPressed(_ sender: UIButton) {
        
        self.delegate?.accepted(section: sender.tag)
    }
    
    @IBAction func declinePressed(_ sender: UIButton) {
        
        self.delegate?.declined(section: sender.tag)
    }
    
    @IBAction func alwaysPressed(_ sender: UIButton) {
        
        self.delegate?.alwaysSelected(section: sender.tag)
    }
    
    @IBAction func onlyPressed(_ sender: UIButton) {
        
        self.delegate?.onlySelected(section: sender.tag)
    }
    
}
