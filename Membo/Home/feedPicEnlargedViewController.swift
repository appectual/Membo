//
//  feedPicEnlargedViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

protocol backFromEnlarge: class {
    func backFromEnlargePressed(feedBack: Feed, row: Int)
}

class feedPicEnlargedViewController: UIViewController, UIScrollViewDelegate {
    
    weak var delegate: backFromEnlarge?

    @IBOutlet weak var feedPic: UIImageView!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var expressButton: UIButton!
    @IBOutlet weak var sep: UIView!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var overlay: UIView!
    
    var feed : Feed?
    var row = Int()
    var feedImage = UIImage()
    var isTapped = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapped(_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        self.tabBarController?.tabBar.frame.origin.y += 49
        self.bottomConstraint.constant -= 49
        
        showAnimation()
        feedPic.image = feedImage
        message.text = feed?.title
        likeCount.text = feed?.likeCount
        commentCount.text = (feed?.commentCount)! + " Expressions"
        
        if feed?.islike == "1" {
            
            likeButton.setTitle("LIKED", for: .normal)
            likeButton.setImage(#imageLiteral(resourceName: "likedByHome"), for: .normal)
        }
        else {
            
            likeButton.setTitle("LIKE", for: .normal)
            likeButton.setImage(#imageLiteral(resourceName: "likeHome"), for: .normal)
        }
        
        makeNavTransparent(navBar)
        handleTap()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return feedPic
    }

    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                self.removeAnimation()
                self.delegate?.backFromEnlargePressed(feedBack: feed!, row: row)
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func tapped(_ gesture: UIGestureRecognizer) {
        
        handleTap()
    }
    
    func handleTap() {
        
        if isTapped {
            
            //hide
            isTapped = false
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
                
                //                self.topConstraint.constant = -64
                //                self.bottomConstraint.constant = -200
                
                self.overlay.alpha = 0
                self.navBar.alpha = 0
                self.message.alpha = 0
                self.likeButton.alpha = 0
                self.expressButton.alpha = 0
                self.sep.alpha = 0
                self.likeCount.alpha = 0
                self.commentCount.alpha = 0
                self.likeImage.alpha = 0
                
            }, completion: { (result) in
                
            })
        }
        else {
            
            //unhide
            isTapped = true
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
                
                //                self.topConstraint.constant = 0
                //                self.bottomConstraint.constant = 6
                
                self.overlay.alpha = 0.2
                self.navBar.alpha = 1
                self.message.alpha = 1
                self.likeButton.alpha = 1
                self.expressButton.alpha = 1
                self.sep.alpha = 1
                self.likeCount.alpha = 1
                self.commentCount.alpha = 1
                self.likeImage.alpha = 1
                
            }, completion: { (result) in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    
                    self.handleTap()
                })
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.barStyle = .default
    }
    
    func makeNavTransparent(_ nav: UINavigationBar) {
        
        nav.setBackgroundImage(UIImage(), for: .default)
        nav.shadowImage = UIImage()
        nav.isTranslucent = true
    }
    
    func showAnimation() {
        
        //zoom in animation for popup
        self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        
        //zoom out for closing view
        self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.view.alpha = 1
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0
            self.view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }) { (result) in
            self.view.removeFromSuperview()
        }
    }

    func likeEnlarge() {
        
        let enlargeImageView = UIImageView(frame: CGRect(x: self.view.frame.size.width/2 - 20, y: feedPic.frame.size.height/2 + 20, width: 40, height: 40))
        enlargeImageView.image = #imageLiteral(resourceName: "likeInside")
        enlargeImageView.alpha = 0
        feedPic.addSubview(enlargeImageView)
        
        enlargeImageView.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            
            enlargeImageView.alpha = 1
            enlargeImageView.transform = CGAffineTransform(scaleX: 5, y: 5)
            
        }) { (result) in
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                
                enlargeImageView.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
                
            }) { (result) in
                
                enlargeImageView.alpha = 0
                enlargeImageView.removeFromSuperview()
            }
        }
    }
    
    func startRotating(duration: Double = 0.3, view: UIView) {
        let kAnimationKey = "rotation"
        
        if view.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = duration
            animate.repeatCount = 2
            animate.fromValue = Float(Double.pi * 2.0)
            animate.toValue = 0.0
            animate.delegate = self
            view.layer.add(animate, forKey: kAnimationKey)
        }
    }
    
    @IBAction func likePressed(_ sender: Any) {

        self.startRotating(view: likeButton.imageView!)
            
        self.postServer2(param: ["user_id": self.getUserId(), "feed_id": feed?.feed_id], url: K.MEMBO_ENDPOINT + "postUserLikePhoto") { (json) in
            
            if json["error"].stringValue == "false" {
                
                
            }
            else if json["error"].stringValue == "true" {
                
                self.Toast(text: "Something went wrong. Try again later.")
            }
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        removeAnimation()
        self.delegate?.backFromEnlargePressed(feedBack: feed!, row: row)
    }
    
    @IBAction func expressPressed(_ sender: UIButton) {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "expressVC") as? expressViewController {
            
            vc.feedId = (feed?.feed_id)!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension feedPicEnlargedViewController: CAAnimationDelegate {
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
        let kAnimationKey = "rotation"
        
        if view.layer.animation(forKey: kAnimationKey) != nil {
            view.layer.removeAnimation(forKey: kAnimationKey)
        }
        
        if feed?.islike == "0" {
            
            self.likeEnlarge()
            feed?.islike = "1"
            feed?.likeCount = String(Int((feed?.likeCount)!)! + 1)
            likeButton.setImage(#imageLiteral(resourceName: "likedByHome"), for: .normal)
            likeButton.setTitle("LIKED", for: .normal)
        }
        else {
            
            feed?.islike = "0"
            feed?.likeCount = String(Int((feed?.likeCount)!)! - 1)
            likeButton.setImage(#imageLiteral(resourceName: "likeHome"), for: .normal)
            likeButton.setTitle("LIKE", for: .normal)
        }
        
        likeCount.text = feed?.likeCount
    }
}
