//
//  locateRideViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import UIEmptyState

class locateRideViewController: UIViewController, UIEmptyStateDelegate, UIEmptyStateDataSource {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var rideTable: UITableView!
    @IBOutlet weak var driverPic: UIImageView!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var driverDesignation: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var locateRideButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableTop: NSLayoutConstraint!
    @IBOutlet weak var requestButton: UIButton!
    
    var isDeparture = Bool()
    var ride : Ride?
    var rowCount = Int()
    var isDriverAssigned = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerView.isHidden = true
        self.locateRideButton.isHidden = true
        
        if isDeparture {
            
            self.addSpacingToTitle(navBar, title: "MY DEPARTURE RIDE")
        }
        else {
            
            self.addSpacingToTitle(navBar, title: "MY ARRIVAL RIDE")
        }
        
        self.headerView.isHidden = true
        tableTop.constant = 0
        self.locateRideButton.isHidden = true
        getDetail()
        
        setupTable()
        
        makeItRound(driverPic, radius: 22.5)
        makeItRound(phoneButton, radius: 20)
        makeItRound(locateRideButton, radius: 20.5)
        makeItRound(requestButton, radius: 20.5)
        fillInfo()
        self.emptyStateDataSource = self
        self.emptyStateDelegate = self
    }
    
    @IBAction func callPressed(_ sender: UIButton) {
        
        SinchCall.sharedInstance.callUser((ride?.dId)!, name: (ride?.dName)!, profile: driverPic.image!)
    }
    
    func getDetail() {
        
        var isPickup = Int()
        
        if isDeparture {
            
            isPickup = 1
        }
        else {
            
            isPickup = 0
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getMyRequest/\(self.getUserId())/\(self.getWeddingId())/\(isPickup)") { [unowned self] (json) in
            
            print(json)
            
            if json["error"].stringValue == "false" {
                
                self.rowCount = 3
                self.requestButton.isHidden = true
                
                if json["userRequest"]["driverData"]["gender"].stringValue != "" {
                    
                    self.rowCount = 5
                    self.tableTop.constant = 75
                    self.isDriverAssigned = true
                    self.headerView.isHidden = false
                    self.driverName.text = json["userRequest"]["driverData"]["driverName"].stringValue
                    self.driverPic.sd_setImage(with: URL(string: json["userRequest"]["driverData"]["profilePic"].stringValue), placeholderImage: #imageLiteral(resourceName: "images"))
                    self.driverDesignation.text = json["userRequest"]["driverData"]["gender"].stringValue
                }
                
                self.ride = Ride(pdId: json["userRequest"]["pdId"].stringValue, userId: json["userRequest"]["userId"].stringValue, pickupDropLocation: json["userRequest"]["pickupDropLocation"].stringValue, pdGeoLocation: json["userRequest"]["pdGeoLocation"].stringValue, flightNumber: json["userRequest"]["flightNumber"].stringValue, flightName: json["userRequest"]["flightName"].stringValue, latitude: json["userRequest"]["latitude"].stringValue, longitude: json["userRequest"]["longitude"].stringValue, driverId: json["userRequest"]["driverId"].stringValue, pdsId: json["userRequest"]["pdsId"].stringValue, statusText: json["userRequest"]["statusText"].stringValue, date: json["userRequest"]["date"].stringValue, time: json["userRequest"]["time"].stringValue, dGender: json["userRequest"]["driverData"]["gender"].stringValue, dName: json["userRequest"]["driverData"]["driverName"].stringValue, cName: json["userRequest"]["driverData"]["carName"].stringValue, dPic: json["userRequest"]["driverData"]["profilePic"].stringValue, dId: json["userRequest"]["driverData"]["user_id"].stringValue, dMobile: json["userRequest"]["driverData"]["mobileNumber"].stringValue, cNumber: json["userRequest"]["driverData"]["carNumber"].stringValue, cType: json["userRequest"]["driverData"]["carType"].stringValue, cColor: json["userRequest"]["driverData"]["carColor"].stringValue)
                
                DispatchQueue.main.async {
                    
                    self.rideTable.reloadData()
                    self.reloadEmptyStateForTableView(self.rideTable)
                }
            }
            else {
                
                DispatchQueue.main.async {
                    
                    self.rideTable.reloadData()
                    self.reloadEmptyStateForTableView(self.rideTable)
                    let button = (self.emptyStateView as? UIEmptyStateView)?.button
                    button?.frame.size.width = 35
                    button?.layer.cornerRadius = 10
                    button?.backgroundColor = self.UIColorFromHex(rgbValue: 0xdc143c)
                }
            }
        }
    }
    
    @IBAction func locatePressed(_ sender: Any) {
        
//        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
//
//            UIApplication.shared.open(NSURL(string:
//                "comgooglemaps://?saddr=&daddr=\(ride?.latitude),\(ride?.longitude)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
//
//        } else {
//
//            if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL)) {
//
//                UIApplication.shared.open(NSURL(string:
//                    "https://maps.google.com/?saddr=&daddr=\(ride?.latitude),\(ride?.longitude)&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
//            }
//        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Some custom button stuff
        let button = (self.emptyStateView as? UIEmptyStateView)?.button
        button?.layer.cornerRadius = 10
//        button?.frame.size.width = 200
//        button?.frame.size.height = 41
        button?.backgroundColor = UIColorFromHex(rgbValue: 0xdc143c)
    }
    
    
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "hotelBig")
    }
    
    var emptyStateTitle: NSAttributedString {
        
        let attrs = [NSForegroundColorAttributeName: UIColor.black,
                     NSFontAttributeName: UIFont(name: "OpenSans-SemiBold", size: 20)]
        return NSAttributedString(string: "No Rides Found", attributes: attrs)
    }
    
    var emptyStateDetailMessage: NSAttributedString? {
        
        let attrs = [NSForegroundColorAttributeName: UIColorFromHex(rgbValue: 0x333333),
                     NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 16)]
        
        if isDeparture {
            
            return NSAttributedString(string: "Looks like you have still not requested for your departure ride", attributes: attrs)
        }
        else {
            
            return NSAttributedString(string: "Looks like you have still not requested for your arrival ride", attributes: attrs)
        }
    }
    
    @IBAction func requestPressed(_ sender: UIButton) {
        
        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
        
        if isDeparture {
            
            if let vc = homeTab.instantiateViewController(withIdentifier: "requestRideVC") as? requestRideViewController {
                
                vc.delegate = self
                vc.isDeparture = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else {
            
            if let vc = homeTab.instantiateViewController(withIdentifier: "requestRideVC") as? requestRideViewController {
                
                vc.delegate = self
                vc.isDeparture = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
//    var emptyStateButtonTitle: NSAttributedString? {
//        let attrs = [NSForegroundColorAttributeName: UIColor.white,
//                     NSFontAttributeName: UIFont(name: "OpenSans-SemiBold", size: 20)]
//        return NSAttributedString(string: "     Request Now     ", attributes: attrs)
//    }
//
//    func emptyStatebuttonWasTapped(button: UIButton) {
//
//        let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
//
//        if isDeparture {
//
//            if let vc = homeTab.instantiateViewController(withIdentifier: "requestRideVC") as? requestRideViewController {
//
//                vc.delegate = self
//                vc.isDeparture = true
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
//        else {
//
//            if let vc = homeTab.instantiateViewController(withIdentifier: "requestRideVC") as? requestRideViewController {
//
//                vc.delegate = self
//                vc.isDeparture = false
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
//    }
    
    
    
    func fillInfo() {
        
        driverName.text = "John Doe"
        driverDesignation.text = "Driver"
        driverPic.image = #imageLiteral(resourceName: "bride_groom_img.jpg")
    }

    func makeItRound(_ view: UIView, radius: CGFloat) {
        
        view.layer.cornerRadius = self.view.frame.size.width * radius/414
        view.clipsToBounds = true
    }
    
    func setupTable() {
        
        rideTable.estimatedRowHeight = 40
        rideTable.rowHeight = UITableViewAutomaticDimension
        rideTable.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 414, height: 60))
    }
}

extension locateRideViewController: rideArranged {
    
    func rideIt() {
        
        getDetail()
    }
}

extension locateRideViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !isDriverAssigned {
            
            if indexPath.row == 0 || indexPath.row == 2 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "singlePane", for: indexPath) as! singlePaneTableViewCell
                
                cell.title.addTextSpacing(spacing: 1.08)
                cell.detail.addTextSpacing(spacing: 1.08)

                if indexPath.row == 0 {
                    
                    setTitleToLabel(cell.title, textName: "Pickup Location")
                    setTitleToLabel(cell.detail, textName: (ride?.pickupDropLocation)!)
                }
                else {
                    
                    setTitleToLabel(cell.title, textName: "Drop Location")
                    setTitleToLabel(cell.detail, textName: (ride?.pickupDropLocation)!)
                }
                
                cell.selectionStyle = .none
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "doublePane", for: indexPath) as! doublePaneTableViewCell
                
                cell.one.addTextSpacing(spacing: 1.08)
                cell.two.addTextSpacing(spacing: 1.08)
                cell.three.addTextSpacing(spacing: 1.08)
                cell.four.addTextSpacing(spacing: 1.08)
                    
                setTitleToLabel(cell.one, textName: "Pickup Date")
                setTitleToLabel(cell.two, textName: (ride?.date)!)
                setTitleToLabel(cell.three, textName: "Pickup Time")
                setTitleToLabel(cell.four, textName: (ride?.time)!)
                
                cell.selectionStyle = .none
                return cell
            }
        }
        else {
            
            if indexPath.row % 2 == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "singlePane", for: indexPath) as! singlePaneTableViewCell
                
                cell.title.addTextSpacing(spacing: 1.08)
                cell.detail.addTextSpacing(spacing: 1.08)
                
                if indexPath.row == 0 {
                    
                    setTitleToLabel(cell.title, textName: "Car Name")
                    setTitleToLabel(cell.detail, textName: (ride?.cName)!)
                }
                else if indexPath.row == 2 {
                    
                    setTitleToLabel(cell.title, textName: "Pickup Location")
                    setTitleToLabel(cell.detail, textName: (ride?.pickupDropLocation)!)
                }
                else {
                    
                    setTitleToLabel(cell.title, textName: "Drop Location")
                    setTitleToLabel(cell.detail, textName: (ride?.pickupDropLocation)!)
                }
                
                cell.selectionStyle = .none
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "doublePane", for: indexPath) as! doublePaneTableViewCell
                
                cell.one.addTextSpacing(spacing: 1.08)
                cell.two.addTextSpacing(spacing: 1.08)
                cell.three.addTextSpacing(spacing: 1.08)
                cell.four.addTextSpacing(spacing: 1.08)
                
                if indexPath.row == 1 {
                    
                    setTitleToLabel(cell.one, textName: "Car Number")
                    setTitleToLabel(cell.two, textName: (ride?.cNumber)!)
                    setTitleToLabel(cell.three, textName: "Car Color")
                    setTitleToLabel(cell.four, textName: (ride?.cColor)!)
                }
                else {
                    
                    setTitleToLabel(cell.one, textName: "Pickup Date")
                    setTitleToLabel(cell.two, textName: (ride?.date)!)
                    setTitleToLabel(cell.three, textName: "Pickup Time")
                    setTitleToLabel(cell.four, textName: (ride?.time)!)
                }
                
                cell.selectionStyle = .none
                return cell
            }
        }
    }
    
    func setTitleToLabel(_ lab: UILabel, textName: String) {
        
        lab.text = textName
    }
}
