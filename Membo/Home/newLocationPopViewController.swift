//
//  newLocationPopViewController.swift
//  Lets Partii
//
//  Created by Ameya Vichare on 02/09/17.
//  Copyright © 2017 vit. All rights reserved.
//

import UIKit
import GoogleMaps
import NVActivityIndicatorView
import UIEmptyState

protocol locationPass: class {
    
    func passLocation(city: String, detail: String, lat: String, long: String)
}

class newLocationPopViewController: UIViewController, UIEmptyStateDelegate, UIEmptyStateDataSource {
    
    @IBOutlet weak var popView: UIView!
    
    @IBOutlet weak var searchTable: UITableView!
    
    @IBOutlet weak var search: UITextField!
    
    weak var delegate: locationPass?
    
    var results = [SearchResult]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        search.becomeFirstResponder()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.35)
        popView.layer.cornerRadius = 5
        popView.clipsToBounds = true
        search.delegate = self
        search.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        showAnimation()
        
        //        self.emptyStateDataSource = self
        //        self.emptyStateDelegate = self
        self.searchTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    deinit {
        
        print("new location pop deinit")
    }
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "mappin")
    }
    
    var emptyStateTitle: NSAttributedString {
        
        let attrs = [NSForegroundColorAttributeName: UIColorFromHex(rgbValue: 0x000000),
                     NSFontAttributeName: UIFont.systemFont(ofSize: 22)]
        return NSAttributedString(string: "No cities found.", attributes: attrs)
    }
    
    var emptyStateDetailMessage: NSAttributedString? {
        
        let attrs = [NSForegroundColorAttributeName: UIColor.black,
                     NSFontAttributeName: UIFont.systemFont(ofSize: 16)]
        return NSAttributedString(string: "Try searching for a different city.", attributes: attrs)
        
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        
        removeAnimation()
    }
    
    func showAnimation() {
        
        //zoom in animation for popup
        self.view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.2) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        
        //zoom out for closing view
        self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.view.alpha = 1
        UIView.animate(withDuration: 0.2, animations: {
            self.view.alpha = 0
            self.view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (result) in
            self.view.removeFromSuperview()
        }
    }
    
    func hitQuery(_ query: String) {
        
        let refinedQuery = query.replacingOccurrences(of: " ", with: "%20")
        
        self.results.removeAll()
        DispatchQueue.main.async {
            self.searchTable.reloadData()
            self.reloadEmptyStateForTableView(self.searchTable)
        }
        
        self.getServer2(url: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(refinedQuery)&key=AIzaSyCXKkcpz3-TktVF7eW4YPzEmerNulAZW9E") { [unowned self] (json) in
            
            print(json)
            
            for item in json["predictions"].arrayValue {
                
                self.results.append(SearchResult(name: item["description"].stringValue, image: #imageLiteral(resourceName: "images"), type: 1, id: 0, lat: "", long: "", city: item["structured_formatting"]["main_text"].stringValue))
                
                DispatchQueue.main.async {
                    
                    self.searchTable.reloadData()
                    self.reloadEmptyStateForTableView(self.searchTable)
                }
            }
        }
    }
    
    
}

extension newLocationPopViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var res: SearchResult
        res = results[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath)
        
        cell.imageView?.image = #imageLiteral(resourceName: "maps-and-flags")
        cell.textLabel?.text = res.name
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let window = UIApplication.shared.keyWindow!
        let frame = CGRect(x: (self.view.frame.size.width - 50)/2, y: (self.view.frame.size.height - 50)/2, width: 50, height: 50)
        let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                            type: NVActivityIndicatorType(rawValue: 3)!)
        var overlay = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        window.addSubview(overlay)
        window.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        
        //pass data back
        var res: SearchResult
        res = results[indexPath.row]
        
        let location = res.name.components(separatedBy: ",")
        let geoCoder = CLGeocoder()
        
        if location.count >= 2 {
            
            geoCoder.geocodeAddressString(res.name) { [unowned self] (placemarks, error) in
                
                let loc = placemarks?.first?.location
                
                let latName = "\(loc!.coordinate.latitude)"
                let longName = "\(loc!.coordinate.longitude)"
                
                overlay.removeFromSuperview()
                activityIndicatorView.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                
                
                self.delegate?.passLocation(city: res.city, detail: location[1], lat: latName, long: longName)
                
                self.removeAnimation()
            }
        }
    }
}

extension newLocationPopViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        textField.text = ""
        
        if !searchTable.isHidden {
            dismissSearchTable()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (textField.text?.characters.count)! >= 3 {
            
            revealSearchTable()
            
            hitQuery(textField.text!)
        }
    }
    
    func textFieldDidChange(_ textfield: UITextField) {
        
        if (textfield.text?.characters.count)! >= 3 {
            
            revealSearchTable()
            
            hitQuery(textfield.text!)
        }
        else if (textfield.text?.characters.count)! == 0 {
            
            dismissSearchTable()
        }
    }
    
    func revealSearchTable() {
        results.removeAll()
        DispatchQueue.main.async {
            self.searchTable.reloadData()
        }
        self.searchTable.alpha = 0
        self.searchTable.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.searchTable.alpha = 1
        }) { (result) in
        }
    }
    
    func dismissSearchTable() {
        
        self.searchTable.alpha = 1
        self.searchTable.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.searchTable.alpha = 0
        }) { (result) in
            
            self.searchTable.isHidden = true
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == searchTable {
            
            search.resignFirstResponder()
        }
    }
    
}
