//
//  rsvpViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class rsvpViewController: UIViewController {

    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var names: UILabel!
    @IBOutlet weak var defaultLab: UILabel!
    @IBOutlet weak var respondLab: UILabel!
    @IBOutlet weak var middleConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var cons1: NSLayoutConstraint!
    @IBOutlet weak var cons2: NSLayoutConstraint!
    
    var isRSVP = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        checkRsvp()
        self.addSpacingToTitle(navBar, title: "RSVP")
        setupButtons(declineButton, border: true)
        setupButtons(acceptButton, border: false)
        setupBrideName()
    }
    
    func checkRsvp() {
        
        if let rsvp = UserDefaults.standard.value(forKey: "rsvpStatus") as? String {
            
            if rsvp == "1" {
                
                isRSVP = 1
            }
            else if rsvp == "2" {
                
                isRSVP = 2
                acceptButton.isHidden = true
                declineButton.isHidden = true
                respondLab.isHidden = true
            }
            else if rsvp == "0" {
                
                isRSVP = 0
            }
        }
    }
    
    @IBAction func acceptPressed(_ sender: Any) {
        
        if isRSVP == 0 {
            
            self.performSegue(withIdentifier: "rsvpDetails", sender: self)
        }
        else if isRSVP == 1 {
            
            let tab = UIStoryboard(name: "TabBar", bundle: nil)
            if let vc = tab.instantiateViewController(withIdentifier: "facilityVC") as? facilitiesViewController {
                
                vc.isRSVP = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func declinePressed(_ sender: Any) {
        
        self.postServer(param: ["userId": self.getUserId(), "weddingId": self.getWeddingId(), "adultCount": "0", "childCount": "0", "rsvpStatus": 2], url: K.MEMBO_ENDPOINT + "submitRsvp", completion: { [unowned self] (json) in
            
            if json["error"].stringValue == "false" {
                
                UserDefaults.standard.set("2", forKey: "rsvpStatus")
                self.acceptButton.isHidden = true
                self.declineButton.isHidden = true
                self.respondLab.isHidden = true
            }
            else {
                
                self.Toast(text: "Something wen't wrong. Try again later.")
            }
        })
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupButtons(_ button: UIButton, border: Bool) {
        
        button.layer.cornerRadius = 20.5
        button.clipsToBounds = true
        
        if border {
            
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColorFromHex(rgbValue: 0x666666).cgColor
        }
    }
    
    func setupBrideName() {
        
        if isRSVP == 0 || isRSVP == 2 {
            
            let attrs1 = [NSFontAttributeName : UIFont(name: "OpenSans-Regular", size: 28.75), NSForegroundColorAttributeName : UIColorFromHex(rgbValue: 0xdc143c)]
            
            let attrs2 = [NSFontAttributeName : UIFont(name: "OpenSans-Regular", size: 28.75), NSForegroundColorAttributeName : UIColorFromHex(rgbValue: 0xdc143c)]
            
            let attributedString1 = NSMutableAttributedString(string:"Hardik\n", attributes:attrs1)
            
            let attributedString2 = NSMutableAttributedString(string:"&\n", attributes:attrs2)
            
            let attributedString3 = NSMutableAttributedString(string:"Malvi", attributes:attrs1)
            
            attributedString1.append(attributedString2)
            attributedString1.append(attributedString3)
            names.attributedText = attributedString1
        }
        else if isRSVP == 1 {
            
            names.text = "THANK YOU !!!"
            defaultLab.text = "Visit the My Facilities section to arrange a ride for yourself"
            acceptButton.setTitle("MY FACILITIES", for: .normal)
            acceptButton.frame.origin.x = self.view.frame.size.width/2 - 132/2
            declineButton.isHidden = true
            respondLab.isHidden = true
            middleConstraint.constant = 0
            cons1.constant = 0
            cons2.constant = 0
        }
        
    }

}
