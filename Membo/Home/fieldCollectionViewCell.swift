//
//  fieldCollectionViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 10/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class fieldCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var lab: UILabel!
    
}
