//
//  requestRideViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 12/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import TextFieldEffects

protocol rideArranged {
    
    func rideIt()
}

class requestRideViewController: UIViewController {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var rideTable: UITableView!
    @IBOutlet weak var arrangeButton: UIButton!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    var isDeparture = Bool()
    var rideDetail = [String](repeating: "", count: 5)
    var delegate: rideArranged?
    var autofill = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        makeItRound()
        
        if !isDeparture {
            
            self.addSpacingToTitle(navBar, title: "REQUEST ARRIVAL RIDE")
        }
        else {
            
            header.text = "Please enter your flight/departure details correctly so we can arrange a cab to drop you"
            self.addSpacingToTitle(navBar, title: "REQUEST DEPARTURE RIDE")
        }
        
        setupTable()
    }
    
    func makeItRound() {
        
        arrangeButton.layer.cornerRadius = 20.5
        arrangeButton.clipsToBounds = true
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func handleTimePicker(sender: UIDatePicker) {
        
        let indexPath = IndexPath(row: 1, section: 0)
        let cell = rideTable.cellForRow(at: indexPath) as! doubleHoshiTableViewCell
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        cell.second.text = dateFormatter.string(from: sender.date)
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        
        let indexPath = IndexPath(row: 1, section: 0)
        let cell = rideTable.cellForRow(at: indexPath) as! doubleHoshiTableViewCell
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        cell.first.text = dateFormatter.string(from: sender.date)
    }
    
    func setupTable() {
        
        rideTable.estimatedRowHeight = 80
        rideTable.rowHeight = UITableViewAutomaticDimension
        rideTable.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    @IBAction func arrangePressed(_ sender: Any) {
        
        var count = 0
        
        for i in 0..<rideDetail.count {
            
            if rideDetail[i].isNotEmpty {
                
                count += 1
            }
        }
        
        print(count)
        print(rideDetail)
        
        if count == 5 {

            var isPickup = String()
            
            if isDeparture {
                
                isPickup = "1"
            }
            else {
                
                isPickup = "0"
            }
            
            let lat = LocationService.sharedInstance.currentLocation?.coordinate.latitude
            let long = LocationService.sharedInstance.currentLocation?.coordinate.longitude
            
            self.postServer(param: ["userId":self.getUserId(), "weddingId":self.getWeddingId(), "isPickup": isPickup, "pickupLocation": rideDetail[4], "date": rideDetail[2], "time": rideDetail[3], "pdGeoLocation": rideDetail[4], "flightNumber": rideDetail[0], "flightName": rideDetail[1], "latitude": "\(lat!)", "longitude": "\(long!)"], url: K.MEMBO_ENDPOINT + "postMyRequest", completion: { [unowned self] (json) in
                
                if json["error"].stringValue == "false" {
                    
                    self.Toast(text: "Your ride details have been submitted.")
                    self.delegate?.rideIt()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                else {
                    
                    self.Toast(text: "Something went wrong. Try again later.")
                }
            })
        }
        else {
            
            self.Toast(text: "Fill in all the details")
        }
    }
}

extension requestRideViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 || indexPath.row == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "double", for: indexPath) as! doubleHoshiTableViewCell
            handleTextfield(cell.first)
            handleTextfield(cell.second)
            cell.first.delegate = self
            cell.second.delegate = self
            
            if indexPath.row == 0 {
                
                cell.first.tag = 0
                cell.second.tag = 1
                cell.first.placeholder = "Flight No."
                cell.second.placeholder = "Flight Name"
            }
            else {
                
                cell.first.tag = 2
                cell.second.tag = 3
                
                if isDeparture {
                    
                    cell.first.placeholder = "Date of Departure"
                    cell.second.placeholder = "Time of Departure"
                }
                else {
                    
                    cell.first.placeholder = "Date of Arrival"
                    cell.second.placeholder = "Time of Arrival"
                }
            }

            cell.selectionStyle = .none
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "single", for: indexPath) as! doubleHoshiTableViewCell
            handleTextfield(cell.first)
            cell.first.tag = 4
            cell.first.delegate = self
            cell.first.isUserInteractionEnabled = false
            cell.first.text = autofill
            
            if isDeparture {
                cell.first.placeholder = "Departing At / Drop Location"
            }
            else {
                cell.first.placeholder = "Arriving At / Pickup Location"
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 2 {
            
            if let popOverVC = storyboard?.instantiateViewController(withIdentifier: "locationPopUp") as? newLocationPopViewController {
                self.addChildViewController(popOverVC)
                
                popOverVC.delegate = self
                popOverVC.view.frame = self.view.frame
                self.view.addSubview(popOverVC.view)
                self.didMove(toParentViewController: self)
            }
        }
    }
    
    func handleTextfield(_ lab: HoshiTextField) {
        
        lab.placeholderColor = UIColorFromHex(rgbValue: 0x999999)
        lab.textColor = UIColor.black
        lab.borderInactiveColor = UIColorFromHex(rgbValue: 0x999999)
        lab.borderActiveColor = UIColorFromHex(rgbValue: 0x999999)
        lab.placeholderFontScale = 0.9
    }
}

extension requestRideViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        rideDetail[textField.tag] = textField.text!
        self.topConstraint.constant = 5
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 0 || textField.tag == 1 {
            
            self.topConstraint.constant = 5
        }
        else if textField.tag == 2 || textField.tag == 3 {
         
            self.topConstraint.constant = -100
        }
        else {
            
            
        }
        
        if textField.tag == 2 {
            
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .date
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }
        else if textField.tag == 3 {
            
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = .time
            datePickerView.tag = textField.tag
            textField.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleTimePicker(sender:)), for: .valueChanged)
        }
    }
}

extension requestRideViewController: locationPass {
    
    func passLocation(city: String, detail: String, lat:String, long:String) {
        
        rideDetail[4] = city + detail
        autofill = city + detail
        
        DispatchQueue.main.async {
            
            self.rideTable.reloadData()
        }
        
        print(city)
        print(detail)
        print(lat)
        print(long)
    }
}


