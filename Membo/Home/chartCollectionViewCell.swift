//
//  chartCollectionViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 10/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import PieCharts

class chartCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var chartView: PieChart!
}
