//
//  locReqViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 08/12/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import UIEmptyState

class locReqViewController: UIViewController, UIEmptyStateDataSource, UIEmptyStateDelegate {

    @IBOutlet weak var reqTable: UITableView!
    @IBOutlet weak var navBar: UINavigationBar!
    
    var notif = [NotificationMembo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addSpacingToTitle(navBar, title: "LOCATION REQUEST")
        self.addFiller()
        self.reqTable.estimatedRowHeight = 200
        self.reqTable.rowHeight = UITableViewAutomaticDimension
        self.reqTable.tableFooterView = UIView(frame: CGRect.zero)
        getRequests()
        
        self.emptyStateDelegate = self
        self.emptyStateDataSource = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.emptyStateDataSource = nil
        self.emptyStateDelegate = nil
    }
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "maps-and-flags")
    }
    
    var emptyStateTitle: NSAttributedString {
        
        let attrs = [NSForegroundColorAttributeName: UIColor.black,
                     NSFontAttributeName: UIFont(name: "OpenSans-SemiBold", size: 20)]
        return NSAttributedString(string: "No Location Requests found.", attributes: attrs)
    }
    
    var emptyStateDetailMessage: NSAttributedString? {
        
        let attrs = [NSForegroundColorAttributeName: UIColorFromHex(rgbValue: 0x333333),
                     NSFontAttributeName: UIFont(name: "OpenSans-Regular", size: 16)]
        
        return NSAttributedString(string: "Looks like you haven't received any Location Requests.", attributes: attrs)
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func getRequests() {
        
        notif.removeAll()
        DispatchQueue.main.async {
            
            self.reqTable.reloadData()
            self.reloadEmptyStateForTableView(self.reqTable)
        }
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getLocationTrackingRequest/\(self.getUserId())/\(self.getWeddingId())") { [unowned self] (json) in
            
            print(json)
            
            if json["error"].stringValue == "false" {
                
                for item in json["requests"].arrayValue {
                    
                    self.notif.append(NotificationMembo(notification_id: item["userId"].stringValue, user_id: item["userId"].stringValue, sender_id: "0", notification_type: item["profilePic"].stringValue, description: item["userName"].stringValue, activity: item["dateTime"].stringValue, date_time: item["timesElapsed"].stringValue))
                    
                    DispatchQueue.main.async {
                        
                        self.reqTable.reloadData()
                        self.reloadEmptyStateForTableView(self.reqTable)
                    }
                }
            }
            else {
                
                self.Toast(text: json["message"].stringValue)
            }
        }
    }
}

extension locReqViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return notif.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "req", for: indexPath) as! locReqTableViewCell
        cell.delegate = self
        
        cell.pic.sd_setImage(with: URL(string: notif[indexPath.row].notification_type), placeholderImage: #imageLiteral(resourceName: "images"))
        cell.name.text = notif[indexPath.section].description
        cell.time.text = notif[indexPath.section].date_time
        
        cell.acceptButton.tag = indexPath.section
        cell.declineButton.tag = indexPath.section
        cell.alwaysButton.tag = indexPath.section
        cell.onlyButton.tag = indexPath.section
        
        cell.declineButton.layer.cornerRadius = 18
        cell.declineButton.clipsToBounds = true
        cell.declineButton.layer.borderColor = UIColor.black.cgColor
        cell.declineButton.layer.borderWidth = 1
        
        cell.acceptButton.layer.cornerRadius = 18
        cell.acceptButton.clipsToBounds = true
        
        cell.pic.layer.cornerRadius = 25
        cell.pic.clipsToBounds = true
        
        cell.alwaysButton.setImage(#imageLiteral(resourceName: "radio_default"), for: .normal)
        cell.alwaysButton.setImage(#imageLiteral(resourceName: "radio_selected"), for: .selected)
        
        cell.onlyButton.setImage(#imageLiteral(resourceName: "radio_default"), for: .normal)
        cell.onlyButton.setImage(#imageLiteral(resourceName: "radio_selected"), for: .selected)
        
        if notif[indexPath.section].sender_id == "0" {
            
            cell.alwaysButton.isSelected = false
            cell.onlyButton.isSelected = false
        }
        else if notif[indexPath.section].sender_id == "1" {
            
            cell.alwaysButton.isSelected = true
            cell.onlyButton.isSelected = false
        }
        else if notif[indexPath.section].sender_id == "2" {
            
            cell.alwaysButton.isSelected = false
            cell.onlyButton.isSelected = true
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 15.0
    }
}

extension locReqViewController: locationAccess {
    
    func alwaysSelected(section: Int) {
        
        notif[section].sender_id = "1"
        
        DispatchQueue.main.async {
            
            self.reqTable.reloadData()
        }
    }
    
    func onlySelected(section: Int) {
        
        notif[section].sender_id = "2"
        
        DispatchQueue.main.async {
            
            self.reqTable.reloadData()
        }
    }
    
    func accepted(section: Int) {
        
        if notif[section].sender_id != "0" {
            
            self.postServer(param: ["userId": self.getUserId(), "weddingId": self.getWeddingId(), "friendId": notif[section].user_id, "status": notif[section].sender_id], url: K.MEMBO_ENDPOINT + "ackLocationRequest", completion: { [unowned self] (json) in
                
                if json["error"].stringValue == "false" {
                    
                    self.Toast(text: "Request accepted.")
                    self.getRequests()
                }
                else {
                    
                    self.Toast(text: "Something went wrong. Try again later.")
                }
            })
        }
        else {
            
            self.Toast(text: "Please select one option.")
        }
    }
    
    func declined(section: Int) {
        
        if notif[section].sender_id != "0" {
            
            self.postServer(param: ["userId": self.getUserId(), "weddingId": self.getWeddingId(), "friendId": notif[section].user_id, "status": 3], url: K.MEMBO_ENDPOINT + "ackLocationRequest", completion: { [unowned self] (json) in
                
                if json["error"].stringValue == "false" {
                    
                    self.Toast(text: "Request declined.")
                    self.getRequests()
                }
                else {
                    
                    self.Toast(text: "Something went wrong. Try again later.")
                }
            })
        }
        else {
            
            self.Toast(text: "Please select one option.")
        }
    }
}
