
//
//  SearchResult.swift
//  Lets Partii
//
//  Created by Ameya Vichare on 23/08/17.
//  Copyright © 2017 vit. All rights reserved.
//

import Foundation

class SearchResult {
    
    var name: String
    var image: UIImage
    var type: Int
    var id: Int
    var lat: String
    var long: String
    var city: String
    
    init(name: String, image:UIImage, type: Int, id:Int, lat:String, long: String, city: String) {
        self.name = name
        self.image = image
        self.type = type
        self.id = id
        self.lat = lat
        self.long = long
        self.city = city
    }
}

