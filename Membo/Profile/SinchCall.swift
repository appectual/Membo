//
//  SinchCall.swift
//  Membo
//
//  Created by Ameya Vichare on 16/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation
import SDWebImage

class SinchCall: NSObject {
    static let sharedInstance = SinchCall()

    var sinchClient : SINClient
    
    var callClient: SINCallClient?
    
    var call: SINCall?
    
    var audioController: SINAudioController?
    
    var window = UIApplication.shared.keyWindow
    
    var incomingCall: SINCall?
    
    var mainView = UIView()
    
    var status = UILabel()
    
    var ansCallButton = UIButton()
    
    var endCallButton = UIButton()
    
    var usernam = String()
    
    var profilePic = UIImage()
    
    var receivedParams = [AnyHashable:Any]()
    
    override init() {
        
        var userId = String()
        
        if let id = UserDefaults.standard.value(forKey: "user_id") as? String {
            
            userId = id
        }
        else {
            
            userId = "0"
        }
        
        self.sinchClient = Sinch.client(withApplicationKey: "bea1e261-4e1c-4ca0-9daa-1f10b6031208", applicationSecret: "Eg2wPlMe30STIaSAwni8XQ==", environmentHost: "sandbox.sinch.com", userId: userId)
    }
    
    func establishConnection() {
        
        print("establishing connection")
        sinchClient.setSupportCalling(true)
        sinchClient.enableManagedPushNotifications()
        sinchClient.call().delegate = self
        sinchClient.start()
        sinchClient.delegate = self
        sinchClient.startListeningOnActiveConnection()
        
        if let devToken = UserDefaults.standard.value(forKey: "deviceTokenForSinch") as? Data {
            
            self.sinchClient.registerPushNotificationDeviceToken(devToken, type: SINPushTypeRemote, apsEnvironment: .production)
        }
    }
    
    func closeConnection() {
        
        print("closing connection")
        sinchClient.stopListeningOnActiveConnection()
    }
    
    func getUserMobileSinch() -> String {
        
        if let mob = UserDefaults.standard.value(forKey: "user_mobile") as? String {
            
            return mob
        }
        else {
            
            return ""
        }
    }
    
    func getUsernameSinch() -> String {
        
        if let name = UserDefaults.standard.value(forKey: "user_name") as? String {
            
            return name
        }
        else {
            
            return ""
        }
    }
    
    func getImageUrl() -> String {
        
        if let url = UserDefaults.standard.value(forKey: "user_image_url") as? String {
            
            return url
        }
        else {
            
            return ""
        }
    }
    
    func callUser(_ id: String, name: String, profile: UIImage) {
        
        let param = ["guestName": self.getUsernameSinch(), "guestNumber": self.getUserMobileSinch(), "guestPic": self.getImageUrl()]
    
        print(sinchClient.userId)
        print("call user")
        callClient = sinchClient.call()
        call = callClient?.callUser(withId: id, headers: param)
//        callClient?.delegate = self
        call?.delegate = self
        usernam = name
        profilePic = profile
        handleUI(state: 1)
    }
}

extension SinchCall: SINClientDelegate {
    
    func client(_ client: SINClient!, logMessage message: String!, area: String!, severity: SINLogSeverity, timestamp: Date!) {
        
        print(message)
    }
    
    func clientDidStart(_ client: SINClient!) {
        
        print("clientDidStart")
    }
    
    func clientDidFail(_ client: SINClient!, error: Error!) {
        
        print("clientDidFail")
    }
}

extension SinchCall: SINCallClientDelegate {
    
    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {
        
        print("recieved")
        call.delegate = self
        
        if (call.details.applicationStateWhenReceived == .active) {
            // Show an answer button or similar in the UI
            print("yayy")
            
            receivedParams = call.headers
            
            let soundFilePath: String? = Bundle.main.path(forResource: "progresstone", ofType: "wav")
            // get audio controller from SINClient
            audioController = sinchClient.audioController()
            audioController?.startPlayingSoundFile(soundFilePath, loop: false)
            
            UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil);
            incomingCall = call
            handleUI(state: 0)
            
        } else {
            // Application was in not in the foreground when the call was initially received,
            // and the user has opened the application (e.g. via a Local Notification),
            // which we then interpret as that the user want to answer the call.
            call.answer()
        }
    }
    
    func handleUI(state: Int) {
        
        //0 incoming 1 outgoing
        
        mainView = UIView(frame: CGRect(x: 0, y: 0, width: (self.window?.frame.size.width)!, height: (self.window?.frame.size.height)!))
        mainView.backgroundColor = UIColor.white
        window?.addSubview(mainView)
        
        let membo = UILabel(frame: CGRect(x: (self.window?.frame.size.width)!/2 - 150, y: 60, width: 300, height: 35))
        membo.textAlignment = .center
        membo.text = ""
        membo.font = UIFont(name: "OpenSans-Regular", size: 25)
        membo.textColor = UIColor.lightGray
        mainView.addSubview(membo)
        
        let username = UILabel(frame: CGRect(x: (self.window?.frame.size.width)!/2 - 200, y: 100, width: 400, height: 30))
        username.textAlignment = .center
        username.text = "Voice Call"
        username.textColor = UIColor.lightGray
        username.font = UIFont(name: "OpenSans-Regular", size: 13)
        mainView.addSubview(username)
        
        status = UILabel(frame: CGRect(x: (self.window?.frame.size.width)!/2 - 200, y: 140, width: 400, height: 21))
        status.textAlignment = .center
        status.text = ""
        status.textColor = UIColor.lightGray
        status.font = UIFont(name: "OpenSans-Regular", size: 12)
        mainView.addSubview(status)
        
        let userimage = UIImageView(frame: CGRect(x: 0, y: 0/*(self.window?.frame.size.height)! - (self.window?.frame.size.width)!*/, width: (self.window?.frame.size.width)!, height: (self.window?.frame.size.height)!))
        userimage.contentMode = .scaleAspectFill
        userimage.clipsToBounds = true
        
        if state == 1 {
            
            membo.text = usernam
            userimage.image = profilePic
            status.text = ""
        }
        else {
            
            if let img = receivedParams["guestPic"] as? String {
                
                userimage.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "images"))
            }
            else {
                
                userimage.image = #imageLiteral(resourceName: "images")
            }
            
            if let name = receivedParams["guestName"] as? String {
                
                membo.text = name
            }
            
            status.text = "INCOMING..."
        }
        mainView.addSubview(userimage)
        
        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: userimage.frame.size.width, height: userimage.frame.size.height))
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        mainView.addSubview(overlay)
        
        mainView.bringSubview(toFront: membo)
        mainView.bringSubview(toFront: username)
        mainView.bringSubview(toFront: status)
        
        endCallButton = UIButton(frame: CGRect(x: (self.window?.frame.size.width)!/2 - 115, y: (self.window?.frame.size.height)! - 60, width: 100, height: 40))
        endCallButton.addTarget(self, action: #selector(self.endCall), for: .touchUpInside)
        endCallButton.layer.cornerRadius = 20
        endCallButton.backgroundColor = UIColor(red: 220.0/255.0, green: 20.0/255.0, blue: 60.0/255.0, alpha: 1)
        endCallButton.setTitle("DECLINE", for: .normal)
        endCallButton.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 13.8)
        endCallButton.setTitleColor(.white, for: .normal)
        mainView.addSubview(endCallButton)
        
        if state == 0 {
            
            ansCallButton = UIButton(frame: CGRect(x: (self.window?.frame.size.width)!/2 + 15, y: (self.window?.frame.size.height)! - 60, width: 100, height: 40))
            ansCallButton.addTarget(self, action: #selector(self.ansCall), for: .touchUpInside)
            ansCallButton.layer.cornerRadius = 20
            ansCallButton.backgroundColor = UIColor(red: 220.0/255.0, green: 20.0/255.0, blue: 60.0/255.0, alpha: 1)
            ansCallButton.setTitle("ACCEPT", for: .normal)
            ansCallButton.titleLabel?.font = UIFont(name: "OpenSans-Regular", size: 13.8)
            ansCallButton.setTitleColor(.white, for: .normal)
            mainView.addSubview(ansCallButton)
        }
        else {
            
            endCallButton.setTitle("END", for: .normal)
            endCallButton.frame.origin.x += 65
//            self.window.bringSubview(toFront: endCallButton)
        }
    }
    
    func endCall() {
        
        print("taped")
        audioController?.stopPlayingSoundFile()
        incomingCall?.hangup()
        call?.hangup()
        audioController?.stopPlayingSoundFile()
    }
    
    func ansCall() {
        
        
        print("taped")
        incomingCall?.answer()
        ansCallButton.isHidden = true
        endCallButton.frame.origin.x += 65
        endCallButton.setTitle("END", for: .normal)
        status.text = "CONNECTED"
        audioController?.stopPlayingSoundFile()
    }
    
    func client(_ client: SINCallClient!, localNotificationForIncomingCall call: SINCall!) -> SINLocalNotification! {
    
        print("incoming")
        let notification = SINLocalNotification()
        notification.alertAction = "Answer"
        notification.alertBody = "Incoming call"
        return notification
    }
}

extension SinchCall: SINCallDelegate {
    
    func callDidProgress(_ call: SINCall!) {
        
        print("callDidProgress")
        //called after call user
        status.text = "CALLING..."
        
        let soundFilePath: String? = Bundle.main.path(forResource: "progresstone", ofType: "wav")
        // get audio controller from SINClient
        audioController = sinchClient.audioController()
        audioController?.startPlayingSoundFile(soundFilePath, loop: false)
    }
    
    func callDidEstablish(_ call: SINCall!) {
        
        //answered by someone else
        print("call did establish")
        status.text = "CONNECTED"
        audioController?.stopPlayingSoundFile()
    }
    
    func callDidEnd(_ call: SINCall!) {
        
        print("call did end")
        mainView.removeFromSuperview()
        audioController?.stopPlayingSoundFile()
    }

}
