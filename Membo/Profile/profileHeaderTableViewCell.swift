//
//  profileHeaderTableViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

protocol guestProfileActions {
    
    func guestProfilePress(status: Int)
}

class profileHeaderTableViewCell: UITableViewCell {
    
    var delegate: guestProfileActions?

    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var pic: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var roleLab: UILabel!
    @IBOutlet weak var roleBlack: UILabel!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    @IBOutlet weak var crown: UIImageView!
    @IBOutlet weak var topCon: NSLayoutConstraint!
    
    @IBAction func chatPressed(_ sender: Any) {
        
        self.delegate?.guestProfilePress(status: 1)
    }
    
    @IBAction func callPressed(_ sender: Any) {
        
        self.delegate?.guestProfilePress(status: 2)
    }
    
    @IBAction func locationPressed(_ sender: Any) {
        
        self.delegate?.guestProfilePress(status: 3)
    }
}
