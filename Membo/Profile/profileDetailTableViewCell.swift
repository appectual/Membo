//
//  profileDetailTableViewCell.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

protocol changePass: class {
    
    func changeIt()
}

protocol callPressed: class {
    
    func callIt()
}

class profileDetailTableViewCell: UITableViewCell {
    
    weak var delegate: changePass?
    
    weak var delegateCall: callPressed?

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    
    @IBAction func changePressed(_ sender: Any) {
        
        self.delegate?.changeIt()
    }
    
    @IBAction func callPressed(_ sender: Any) {
        
        self.delegateCall?.callIt()
    }
}
