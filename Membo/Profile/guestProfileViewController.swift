//
//  guestProfileViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SDWebImage

class guestProfileViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var profileTable: UITableView!
    var callManager: CallManager!
    var guest : Guest?
    
    var guestIsOnline = false
    var isPlanner = Bool()
    var isGuestAdmin = Bool()
    var onlineUsers = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("loaded")
        
        if !isPlanner {
            
            self.checkStoredOnlineUsers()
            self.addSpacingToTitle(navBar, title: "PROFILE")
            callManager = AppDelegate.shared.callManager
        }
        else {
            
            if isGuestAdmin {
                
                self.addSpacingToTitle(navBar, title: "PROFILE")
            }
            else {
                
                self.addSpacingToTitle(navBar, title: "PLANNER PROFILE")
            }
        }
        
        setupTable()
    }
    
    @IBAction func morePressed(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: "More", message: nil , preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Block", style: .default , handler: { [unowned self]
            (action: UIAlertAction) -> Void in

            self.postServer(param: ["user_id": self.getUserId(), "friend_id": self.guest?.userId, "wedding_id": self.getWeddingId(), "flag": true], url: K.MEMBO_ENDPOINT + "postBlock", completion: { [unowned self] (json) in
                
                if json["error"].stringValue == "false" {
                    
                    self.Toast(text: "User blocked.")
                }
            })
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: { [unowned self]
            (action: UIAlertAction) -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }))
        
        present(actionSheet, animated: true, completion:nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadOnline(_:)), name: .online, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    func reloadOnline(_ notification: Notification) {
        
        if let userId = notification.object as? [String] {
            
            onlineUsers = userId
            print(userId)
            changeOnlineUI()
        }
    }
    
    func checkStoredOnlineUsers() {
        
        if var users = UserDefaults.standard.value(forKey: "onlineUsers") as? [String] {
            
            for j in 0..<users.count {
                
                if users[j] == (guest?.userId)! {

                    guestIsOnline = true
                }
            }
            
            DispatchQueue.main.async {
                
                self.profileTable.reloadData()
            }
        }
    }
    
    func changeOnlineUI() {
        
        print("xxxxxxxxxxxxxxxx")
        print(onlineUsers)
        print((guest?.userId)!)
            
        for j in 0..<onlineUsers.count {
            
            if onlineUsers[j] == (guest?.userId)! {
                
                print("yyyyyyy")
                
                guestIsOnline = true
            }
        }
        
        DispatchQueue.main.async {
            
            self.profileTable.reloadData()
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupTable()  {
        
        profileTable.estimatedRowHeight = 40
        profileTable.rowHeight = UITableViewAutomaticDimension
    }
}

extension guestProfileViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !isPlanner {
            
            return 5
        }
        else {
            
            if isGuestAdmin {
                
                return 5
            }
            else {
                
                return 4
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileHeader", for: indexPath) as! profileHeaderTableViewCell
            cell.delegate = self
            cell.backImage.sd_setImage(with: URL(string: (guest?.profilePic)!), placeholderImage: #imageLiteral(resourceName: "images"))
            cell.pic.sd_setImage(with: URL(string: (guest?.profilePic)!), placeholderImage: #imageLiteral(resourceName: "images"))
            cell.name.text = guest?.guestName
            cell.pic.layer.borderWidth = 2
            
            if guestIsOnline {
                
                cell.pic.layer.borderColor = UIColor.green.cgColor
            }
            else {
                
                cell.pic.layer.borderColor = UIColor.red.cgColor
            }
            
            cell.roleBlack.addTextSpacing(spacing: 1.20)
            makeItRound(cell.pic, border: false, radius: 57.5, isButton: false)
            makeItRound(cell.callButton, border: false, radius: 28.0, isButton: true)
            makeItRound(cell.chatButton, border: false, radius: 28.0, isButton: true)
            makeItRound(cell.locationButton, border: false, radius: 28.0, isButton: true)
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
            makeItRound(cell.roleBlack, border: false, radius: 28.0, isButton: true)
            
            if isPlanner {
                
                if isGuestAdmin {
                    
                    if guest?.rsvpStatus == "1" {
                        
                        cell.roleBlack.text = "ATTENDING"
                    }
                    else if guest?.rsvpStatus == "2" {
                        
                        cell.roleBlack.text = "NOT ATTENDING"
                    }
                    else {
                        
                        cell.roleBlack.text = "PENDING"
                    }
                    
                    cell.roleLab.text = "RSVP"
                    cell.crown.isHidden = false
                }
                else {
                    
                    cell.crown.isHidden = true
                    cell.roleBlack.text = guest?.location
                }
                
                cell.callButton.isHidden = true
                cell.chatButton.isHidden = true
                cell.locationButton.isHidden = true
                cell.roleLab.isHidden = false
                cell.roleBlack.isHidden = false
                
                cell.location.text = guest?.email
                cell.imgHeight.constant = 286
                cell.topCon.constant = 31
            }
            else {
                
                cell.callButton.isHidden = false
                cell.chatButton.isHidden = false
                cell.locationButton.isHidden = false
                cell.roleLab.isHidden = true
                cell.roleBlack.isHidden = true
                cell.location.text = guest?.location
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else {
            
            if indexPath.row == 2 {
                
                if isPlanner {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "detailTwo", for: indexPath) as! doublePaneTableViewCell
                    
                    cell.one.addTextSpacing(spacing: 1.08)
                    cell.two.addTextSpacing(spacing: 1.08)
                    cell.three.addTextSpacing(spacing: 1.08)
                    cell.four.addTextSpacing(spacing: 1.08)
                    
                    cell.one.text = "Arrival Date"
                    cell.three.text = "Arrival Time"
                    
                    cell.two.text = ""
                    cell.four.text = ""
                    
                    cell.selectionStyle = .none
                    return cell
                }
                else {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! profileDetailTableViewCell
                    cell.title.addTextSpacing(spacing: 1.08)
                    cell.detail.addTextSpacing(spacing: 1.08)
                    cell.title.text = "Arrived From"
                    cell.detail.text = guest?.location
                    cell.selectionStyle = .none
                    return cell
                }
            }
            else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! profileDetailTableViewCell
                
                cell.delegateCall = self
                
                cell.title.addTextSpacing(spacing: 1.08)
                cell.detail.addTextSpacing(spacing: 1.08)
                makeItRound(cell.callButton, border: false, radius: 23.0, isButton: true)
                
                if indexPath.row == 1 {
                    
                    if !isPlanner {
                        
                        if guest?.rsvpStatus == "1" {
                            
                            cell.detail.text = "Yes"
                        }
                        else {
                            
                            cell.detail.text = "No"
                        }
                        
                        cell.title.text = "Attending the wedding"
                        cell.callButton.isHidden = true
                    }
                    else {
                        
                        cell.detail.text = guest?.mobileNo
                        cell.title.text = "Mobile No."
                        cell.callButton.isHidden = false
                    }
                }
                else if indexPath.row == 2 {
                    
                    cell.title.text = "Arrived From"
                    cell.detail.text = guest?.location
                }
                else if indexPath.row == 3 {
                    
                    if isPlanner {
                        
                        if isGuestAdmin {
                            
                            cell.title.text = "Arrival Location"
                            cell.detail.text = ""
                        }
                        else {
                            
                            cell.title.text = "Guests Assigned to"
                            cell.detail.text = guest?.gender
                        }
                    }
                    else {
                        
                        cell.title.text = "Guest From"
                        cell.detail.text = guest?.guestFrom
                    }
                }
                else {
                    
                    if isPlanner && isGuestAdmin {
                        
                        cell.title.text = "Assigned to"
                        cell.detail.text = ""
                    }
                    else {
                        
                        cell.title.text = "Relation with Groom/Bride"
                        cell.detail.text = guest?.guestRelation
                    }
                }
                
                cell.selectionStyle = .none
                return cell
            }
            }
    }
    
    func makeItRound(_ view: UIView, border: Bool, radius: CGFloat, isButton: Bool) {
        
        if isButton {
            
            view.layer.cornerRadius = radius
        }
        else {
            
            view.layer.cornerRadius = self.view.frame.size.width * radius/414
        }
        view.clipsToBounds = true
        
        if border {
            
            view.layer.borderWidth = 2
            view.layer.borderColor = UIColorFromHex(rgbValue: 0xdc143c).cgColor
        }
    }
}

extension guestProfileViewController: callPressed {
    
    func callIt() {
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = profileTable.cellForRow(at: indexPath) as? profileHeaderTableViewCell
        
        SinchCall.sharedInstance.callUser((guest?.userId)!, name: (guest?.guestName)!, profile: (cell?.pic.image!)!)
    }
}

extension guestProfileViewController: guestProfileActions {
    
    func guestProfilePress(status: Int) {
        
        let handle = "111"
        let videoEnabled = false
        
        switch status {
        case 1:
            
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = profileTable.cellForRow(at: indexPath) as? profileHeaderTableViewCell
            
            let tab = UIStoryboard(name: "HomeTab", bundle: nil)
            if let vc = tab.instantiateViewController(withIdentifier: "chatInsideVC") as? chatInsideViewController {

                vc.hisImage = cell?.pic.image!
                vc.hisName = (guest?.guestName)!
                vc.friendId = (guest?.userId)!
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case 2:
            print("voip")
            
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = profileTable.cellForRow(at: indexPath) as? profileHeaderTableViewCell
            
            SinchCall.sharedInstance.callUser((guest?.userId)!, name: (guest?.guestName)!, profile: (cell?.pic.image!)!)
//            let backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
//            DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1.5) {
//                AppDelegate.shared.displayIncomingCall(uuid: UUID(), handle: handle, hasVideo: videoEnabled) { _ in
//                    UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
//                }
//            }
        case 3:
            postLocationRequest()
        default:
            break
        }
    }
    
    func postLocationRequest() {
        
        self.postServer(param: ["userId":self.getUserId(),"weddingId":self.getWeddingId(),"friendId":(self.guest?.userId)!], url: K.MEMBO_ENDPOINT + "postLocationRequest") { [unowned self] (json) in

            
            if json["error"].stringValue == "false" {
                
                switch json["status"].stringValue {
                
                case "1":
                self.Toast(text: "Location request sent")
                    
                case "2":
                self.Toast(text: "Location request is pending")
                    
                case "4":
                
                let tab = UIStoryboard(name: "TabBar", bundle: nil)
                if let vc = tab.instantiateViewController(withIdentifier: "mapVC") as? mapViewController {
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                    
                default:
                    break
                }
            }
        }
    }
}
