//
//  Helpers.swift
//  Membo
//
//  Created by Ameya Vichare on 22/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation
import Toast_Swift
import Alamofire
import SwiftyJSON
import QRCodeReader
import NVActivityIndicatorView

extension UIViewController {
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setTabBarVisible(visible:Bool, animated:Bool) {
        
        if (tabBarIsVisible() == visible) { return }
        
        let frame = self.tabBarController?.tabBar.frame
        let height = frame?.size.height
        let offsetY = (visible ? -height! : height)
        let duration:TimeInterval = (animated ? 0.3 : 0.0)
        if frame != nil {
            UIView.animate(withDuration: duration) {
                self.tabBarController?.tabBar.frame = frame!.offsetBy(dx: 0, dy: offsetY!)
                return
            }
        }
    }
    
    func tabBarIsVisible() ->Bool {
        
        return (self.tabBarController?.tabBar.frame.origin.y)! < self.view.frame.maxY
    }
    
    func handleSplash() {
        
        let appWindow = UIApplication.shared.keyWindow
        
        let backWhite = UIView(frame: CGRect(x: 0, y: 0, width: (self.view.frame.size.width), height: (self.view.frame.size.height)))
        backWhite.backgroundColor = UIColor.white
        appWindow?.addSubview(backWhite)
        
        let spinner = UIImageView(frame: CGRect(x: (self.view.frame.size.width)/2 - 120, y: (self.view.frame.size.height)/2 - 120, width: 240, height: 240))
        backWhite.addSubview(spinner)
        var imgListArray = [UIImage]()
        for countValue in 1...15
        {
            
            let strImageName : String = "logo-\(countValue)"
            let image  = UIImage(named:strImageName)
            imgListArray.append(image!)
        }
        
        spinner.animationImages = imgListArray;
        spinner.animationDuration = 1.5
        spinner.startAnimating()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            
            backWhite.removeFromSuperview()
        }
    }
    
    func getUserId() -> Int {
        
        if let id = UserDefaults.standard.value(forKey: "user_id") as? String {
            
            return Int(id)!
        }
        else {
            
            return 0
        }
    }
    
    func addFiller() {
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor.white
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
    }
    
    func box(text: String, fontSize: CGFloat, fontName: String) -> (CGFloat,CGFloat) {
        
        let someString = text
        let yourFont = UIFont(name: fontName, size: fontSize)
        let stringBoundingBox = someString.size(attributes: [NSFontAttributeName : yourFont])
        
        return (stringBoundingBox.width,stringBoundingBox.height)
    }
    
    func getUserImage() -> UIImage {
        
        if let data = UserDefaults.standard.value(forKey: "user_image") as? Data {
            
            return UIImage(data: data)!
        }
        else {
            
            return #imageLiteral(resourceName: "profile-placeholder")
        }
    }
    
    func isAdmin() -> Int {
        
        if let adm = UserDefaults.standard.value(forKey: "isAdmin") as? Bool {
            
            if adm {
                
                return 1
            }
            else {
                
                if let adm_identifier = UserDefaults.standard.value(forKey: "admin_identifier") as? Int {
                    
                    if adm_identifier == 3 {
                        
                        return 3
                    }
                }
                
                return 0
            }
        }
        else {
            
            return 0
        }
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func getUsername() -> String {
        
        if let name = UserDefaults.standard.value(forKey: "user_name") as? String {
            
            return name
        }
        else {
            
            return ""
        }
    }
    
    func getImageUrl() -> String {
        
        if let url = UserDefaults.standard.value(forKey: "user_image_url") as? String {
            
            return url
        }
        else {
            
            return ""
        }
    }
    
    func getUserMobile() -> String {
        
        if let mob = UserDefaults.standard.value(forKey: "user_mobile") as? String {
            
            return mob
        }
        else {
            
            return ""
        }
    }
    
    func getGender() -> String {
        
        if let gen = UserDefaults.standard.value(forKey: "user_gender") as? String {
            
            return gen
        }
        else {
            
            return ""
        }
    }
    
    func getWeddingId() -> Int {
        
        if let id = UserDefaults.standard.value(forKey: "wedding_id") as? String {
            
            return Int(id)!
        }
        else {
            
            return 0
        }
    }
    
    func Toast(text: String) {
        
        var style = ToastStyle()
        
        style.messageColor = UIColor.white
        
        style.messageAlignment = .center
        
        style.cornerRadius = 18
        
        style.backgroundColor = UIColor.red
        
        style.messageFont = UIFont(name: "Arial", size: 14)!
        
        self.view.makeToast(text, duration: 2.0, position: self.giveToastPoint(), style: style)
    }
    
    func giveToastPoint() -> CGPoint {
        
        return CGPoint(x: self.view.frame.midX, y: self.view.frame.size.height - 150)
    }
    
    func spinnerStart() {
        
        let widthRatio = self.view.frame.size.width * 150/414
        let heightRatio = self.view.frame.size.width * 160/414
        
        let spinner = UIImageView()
        
        var imgListArray = [UIImage]()
        for countValue in 1...6
        {
            
            let strImageName : String = "loading-\(countValue)"
            let image  = UIImage(named:strImageName)
            imgListArray.append(image!)
        }
        
        let mainView = UIView(frame: CGRect(x: self.view.frame.size.width/2 - widthRatio/2, y: self.view.frame.size.height/2 - heightRatio/2, width: widthRatio, height: heightRatio))
        
        spinner.frame = CGRect(x: (mainView.frame.size.width - self.view.frame.size.width * 100/414)/2, y: self.view.frame.size.width * 10/414, width: self.view.frame.size.width * 100/414, height: self.view.frame.size.width * 100/414)
        spinner.animationImages = imgListArray;
        spinner.animationDuration = 1.5
        spinner.tag = 98
        spinner.startAnimating()
        
        mainView.backgroundColor = UIColor.white
        mainView.tag = 99
        mainView.addSubview(spinner)
        
        let waitLabel = UILabel(frame: CGRect(x: (mainView.frame.size.width - self.view.frame.size.width * 100/414)/2, y: self.view.frame.size.width * 130/414, width: self.view.frame.size.width * 100/414, height: self.view.frame.size.width * 25/414))
        waitLabel.text = "Please wait..."
        waitLabel.font = UIFont(name: "OpenSans-Regular", size: 16)
        waitLabel.textColor = UIColor.black
        mainView.addSubview(waitLabel)
        
        self.view.addSubview(mainView)
    }
    
    func stopSpinner() {

        if let spinner = self.view.viewWithTag(98) as? UIImageView {
            spinner.stopAnimating()
        }
        
        if let mainView = self.view.viewWithTag(99) {
            mainView.removeFromSuperview()
        }
    }
    
    func getServer(url: String, completion: @escaping (JSON) -> ()) {
        
        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(overlay)
        spinnerStart()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        var value = String()
        
        if let val = UserDefaults.standard.value(forKey: "user_api") as? String {
            
            value = val
        }
        
        let header = ["APIKEY": value]
        
        Alamofire.request(url, method: .get, headers: header)
            .responseJSON { response in
                
                self.stopSpinner()
                overlay.removeFromSuperview()
                UIApplication.shared.endIgnoringInteractionEvents()

                switch response.result {
                case.success( _):
                    let json = JSON(response.result.value)
                    completion(json)
                case.failure( _):
                    
                    completion(JSON(data: Data()))
                }
        }
    }
    
    func getServer2(url: String, completion: @escaping (JSON) -> ()) {
        
        var value = String()
        
        if let val = UserDefaults.standard.value(forKey: "user_api") as? String {
            
            value = val
        }
        
        let header = ["APIKEY": value]
        
        Alamofire.request(url, method: .get, headers: header)
            .responseJSON { response in
                
                switch response.result {
                case.success( _):
                    let json = JSON(response.result.value)
                    completion(json)
                case.failure( _):
                    
                    completion(JSON(data: Data()))
                }
        }
    }
    
    func getServer3(url: String, completion: @escaping (JSON) -> ()) {
        
        spinnerStart()
        
        var value = String()
        
        if let val = UserDefaults.standard.value(forKey: "user_api") as? String {
            
            value = val
        }
        
        let header = ["APIKEY": value]
        
        Alamofire.request(url, method: .get, headers: header)
            .responseJSON { response in

                self.stopSpinner()
                
                switch response.result {
                case.success( _):
                    let json = JSON(response.result.value)
                    completion(json)
                case.failure( _):
                    
                    completion(JSON(data: Data()))
                }
        }
    }
    
    func postServer(param: [String: Any], url: String, completion: @escaping (JSON) -> ()) {

        let overlay = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.view.addSubview(overlay)
        spinnerStart()
        UIApplication.shared.beginIgnoringInteractionEvents()

        var value = String()
        
        if let val = UserDefaults.standard.value(forKey: "user_api") as? String {
            
            value = val
        }
        let header = ["APIKEY": value]
        
        Alamofire.request(url, method: .post, parameters: param, headers: header)
            .responseJSON { response in
                
                self.stopSpinner()
                overlay.removeFromSuperview()
                UIApplication.shared.endIgnoringInteractionEvents()

                switch response.result {
                case.success( _):
                    let json = JSON(response.result.value)
                    completion(json)
                case.failure( _):
                    
                    completion(JSON(data: Data()))
                }
        }
    }
    
    func postServer2(param: [String: Any], url: String, completion: @escaping (JSON) -> ()) {
        
        var value = String()
        
        if let val = UserDefaults.standard.value(forKey: "user_api") as? String {
            
            value = val
        }
        let header = ["APIKEY": value]
        print(value)
        
        Alamofire.request(url, method: .post, parameters: param, headers: header)
            .responseJSON { response in
                
                switch response.result {
                case.success( _):
                    let json = JSON(response.result.value)
                    completion(json)
                case.failure( _):
                    
                    completion(JSON(data: Data()))
                }
        }
    }
    
    func postServer3(param: [String: Any], url: String, completion: @escaping (JSON) -> ()) {
        
        spinnerStart()
        let window = UIApplication.shared.keyWindow!
        
        var value = String()
        
        if let val = UserDefaults.standard.value(forKey: "user_api") as? String {
            
            value = val
        }
        let header = ["APIKEY": value]
        
        Alamofire.request(url, method: .post, parameters: param, headers: header)
            .responseJSON { response in

                self.stopSpinner()
                
                switch response.result {
                case.success( _):
                    let json = JSON(response.result.value)
                    completion(json)
                case.failure( _):
                    
                    completion(JSON(data: Data()))
                }
        }
    }
    
    func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController?
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert?.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                
                alert?.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            case -11814:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert?.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            default:
                alert = nil
            }
            
            guard let vc = alert else { return false }
            
            present(vc, animated: true, completion: nil)
            
            return false
        }
    }
    
    func addSpacingToTitle(_ nav: UINavigationBar, title: String) {
        
        let titleLabel = UILabel()
        let colour = UIColor.black
        let attributes: [NSString : AnyObject] = [NSFontAttributeName as NSString: UIFont(name: "OpenSans-SemiBold", size: 18.4)!, NSForegroundColorAttributeName as NSString: colour, NSKernAttributeName as NSString : 1.2 as AnyObject]
        titleLabel.attributedText = NSAttributedString(string: title, attributes: attributes as [String : Any])
        titleLabel.sizeToFit()
        nav.topItem?.titleView = titleLabel
    }
    
    
    
    func setupHam(_ nav: UINavigationBar, button: UIBarButtonItem) {
        
        let revealController: SWRevealViewController? = revealViewController()
        _ = revealController?.panGestureRecognizer()
        _ = revealController?.tapGestureRecognizer()
        
        //transparent navbar
        nav.setBackgroundImage(UIImage(), for: .default)
        nav.shadowImage = UIImage()
        nav.isTranslucent = true
        
        button.target = revealViewController()
        button.action = #selector(SWRevealViewController.revealToggle(_:))
        
//        revealViewController().rearViewRevealWidth = self.view.frame.size.width - 69
    }
    
}

public extension UIDevice {
    
    var width: Int {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 , value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return 320
        case "iPod7,1":                                 return 320
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return 320
        case "iPhone4,1":                               return 320
        case "iPhone5,1", "iPhone5,2":                  return 320
        case "iPhone5,3", "iPhone5,4":                  return 320
        case "iPhone6,1", "iPhone6,2":                  return 320
        case "iPhone7,2":                               return 375
        case "iPhone7,1":                               return 414
        case "iPhone8,1":                               return 375
        case "iPhone8,2":                               return 414
        case "iPhone9,1", "iPhone9,3":                  return 375
        case "iPhone9,2", "iPhone9,4":                  return 414
        default:                                        return 0
        }
    }
    
}

public enum Model : String {
    case simulator   = "simulator/sandbox",
    iPod1            = "iPod 1",
    iPod2            = "iPod 2",
    iPod3            = "iPod 3",
    iPod4            = "iPod 4",
    iPod5            = "iPod 5",
    iPad2            = "iPad 2",
    iPad3            = "iPad 3",
    iPad4            = "iPad 4",
    iPhone4          = "iPhone 4",
    iPhone4S         = "iPhone 4S",
    iPhone5          = "iPhone 5",
    iPhone5S         = "iPhone 5S",
    iPhone5C         = "iPhone 5C",
    iPadMini1        = "iPad Mini 1",
    iPadMini2        = "iPad Mini 2",
    iPadMini3        = "iPad Mini 3",
    iPadAir1         = "iPad Air 1",
    iPadAir2         = "iPad Air 2",
    iPadPro9_7       = "iPad Pro 9.7\"",
    iPadPro9_7_cell  = "iPad Pro 9.7\" cellular",
    iPadPro10_5      = "iPad Pro 10.5\"",
    iPadPro10_5_cell = "iPad Pro 10.5\" cellular",
    iPadPro12_9      = "iPad Pro 12.9\"",
    iPadPro12_9_cell = "iPad Pro 12.9\" cellular",
    iPhone6          = "iPhone 6",
    iPhone6plus      = "iPhone 6 Plus",
    iPhone6S         = "iPhone 6S",
    iPhone6Splus     = "iPhone 6S Plus",
    iPhoneSE         = "iPhone SE",
    iPhone7          = "iPhone 7",
    iPhone7plus      = "iPhone 7 Plus",
    iPhone8          = "iPhone 8",
    iPhone8plus      = "iPhone 8 Plus",
    iPhoneX          = "iPhone X",
    unrecognized     = "?unrecognized?"
}

public extension UIDevice {
    public var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
                
            }
        }
        var modelMap : [ String : Model ] = [
            "i386"       : .simulator,
            "x86_64"     : .simulator,
            "iPod1,1"    : .iPod1,
            "iPod2,1"    : .iPod2,
            "iPod3,1"    : .iPod3,
            "iPod4,1"    : .iPod4,
            "iPod5,1"    : .iPod5,
            "iPad2,1"    : .iPad2,
            "iPad2,2"    : .iPad2,
            "iPad2,3"    : .iPad2,
            "iPad2,4"    : .iPad2,
            "iPad2,5"    : .iPadMini1,
            "iPad2,6"    : .iPadMini1,
            "iPad2,7"    : .iPadMini1,
            "iPhone3,1"  : .iPhone4,
            "iPhone3,2"  : .iPhone4,
            "iPhone3,3"  : .iPhone4,
            "iPhone4,1"  : .iPhone4S,
            "iPhone5,1"  : .iPhone5,
            "iPhone5,2"  : .iPhone5,
            "iPhone5,3"  : .iPhone5C,
            "iPhone5,4"  : .iPhone5C,
            "iPad3,1"    : .iPad3,
            "iPad3,2"    : .iPad3,
            "iPad3,3"    : .iPad3,
            "iPad3,4"    : .iPad4,
            "iPad3,5"    : .iPad4,
            "iPad3,6"    : .iPad4,
            "iPhone6,1"  : .iPhone5S,
            "iPhone6,2"  : .iPhone5S,
            "iPad4,1"    : .iPadAir1,
            "iPad4,2"    : .iPadAir2,
            "iPad4,4"    : .iPadMini2,
            "iPad4,5"    : .iPadMini2,
            "iPad4,6"    : .iPadMini2,
            "iPad4,7"    : .iPadMini3,
            "iPad4,8"    : .iPadMini3,
            "iPad4,9"    : .iPadMini3,
            "iPad6,3"    : .iPadPro9_7,
            "iPad6,11"   : .iPadPro9_7,
            "iPad6,4"    : .iPadPro9_7_cell,
            "iPad6,12"   : .iPadPro9_7_cell,
            "iPad6,7"    : .iPadPro12_9,
            "iPad6,8"    : .iPadPro12_9_cell,
            "iPad7,3"    : .iPadPro10_5,
            "iPad7,4"    : .iPadPro10_5_cell,
            "iPhone7,1"  : .iPhone6plus,
            "iPhone7,2"  : .iPhone6,
            "iPhone8,1"  : .iPhone6S,
            "iPhone8,2"  : .iPhone6Splus,
            "iPhone8,4"  : .iPhoneSE,
            "iPhone9,1"  : .iPhone7,
            "iPhone9,2"  : .iPhone7plus,
            "iPhone9,3"  : .iPhone7,
            "iPhone9,4"  : .iPhone7plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,2" : .iPhone8plus,
            "iPhone10,3" : .iPhoneX
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            return model
        }
        return Model.unrecognized
    }
}

extension UILabel {
    func addTextSpacing(spacing: CGFloat){
        let attributedString = NSMutableAttributedString(string: (self.text!))
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: NSRange(location: 0, length: (self.text!.characters.count)))
        self.attributedText = attributedString
    }
}

extension UIButton {
    
    func addTextSpacing(spacing: CGFloat) {
        let attributedString = NSMutableAttributedString(string: (self.titleLabel?.text!)!)
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: NSRange(location: 0, length: (self.titleLabel?.text!.characters.count)!))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}
