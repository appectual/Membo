//
//  Event.swift
//  Membo
//
//  Created by Ameya Vichare on 08/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class Event {
    
    var event_id: String
    var wedding_id: String
    var event_name: String
    var event_location: String
    var event_desc: String
    var event_date_time: String
    var event_date: String
    var event_time: String
    var date: String
    
    init(event_id: String, wedding_id: String, event_name: String, event_location: String, event_desc: String, event_date_time: String, event_date: String, event_time: String, date: String) {
        
        self.event_id = event_id
        self.wedding_id = wedding_id
        self.event_name = event_name
        self.event_location = event_location
        self.event_desc = event_desc
        self.event_date_time = event_date_time
        self.event_date = event_date
        self.event_time = event_time
        self.date = date
    }
}
