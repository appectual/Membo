//
//  ImageLoader.swift
//  Membo
//
//  Created by Ameya Vichare on 03/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation
import UIKit

class ImageLoader {
    
    var cache = NSCache<NSString, NSData>()
    
    class var sharedLoader : ImageLoader {
        struct Static {
            static let instance : ImageLoader = ImageLoader()
        }
        return Static.instance
    }
    
    func imageForUrl(urlString: String, completionHandler:@escaping (_ image: UIImage?, _ url: String) -> ()) {
        
        DispatchQueue.main.async {
            
            let data: Data? = self.cache.object(forKey: urlString as NSString) as Data?
            
            if let goodData = data {
                let image = UIImage(data: goodData)
                
                DispatchQueue.main.async {
                    
                    completionHandler(image, urlString)
                }
                return
            }
            
            let mapsUrl = urlString
            
            if !mapsUrl.isEmpty {
                
                let url = URL(string: mapsUrl)
                
                let task = URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                    
                    if error != nil
                    {
                        print("error")
                        
                        completionHandler(#imageLiteral(resourceName: "profile-placeholder"), urlString)
                        
                        return
                    }
                    else
                    {
                        DispatchQueue.main.async(execute: {
                            
                            if let image = UIImage(data: data!) {
                                
                                //                                print("in image")
                                
                                let dat = NSData(data: data!)
                                
                                self.cache.setObject(dat, forKey: urlString as NSString)
                                
                                completionHandler(image, urlString)
                                
                                return
                                
                            }
                            
                        })
                        
                    }
                    
                    
                })
                task.resume()
                
            }
            else {
                
                completionHandler(#imageLiteral(resourceName: "profile-placeholder"), urlString)
                
                return
            }
            
            
        }
        
        
    }
}
