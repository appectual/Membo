//
//  leftAlignedTextField.swift
//  Membo
//
//  Created by Ameya Vichare on 25/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation
import UIKit

class leftPaddedTextField: UITextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 35, y: bounds.origin.y, width: bounds.width - 70, height: bounds.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 35, y: bounds.origin.y, width: bounds.width - 70, height: bounds.height)
    }
    
    override func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        
        return CGRect(x: bounds.size.width/2-20, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height)
    }
}

