//
//  selectionViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 23/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class selectionViewController: UIViewController {
    
    @IBOutlet weak var driverButton: UIButton!
    @IBOutlet weak var adminButton: UIButton!
    @IBOutlet weak var guestButton: UIButton!
    @IBOutlet weak var navBar: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupButtons(driverButton, border: true)
        setupButtons(adminButton, border: true)
        setupButtons(guestButton, border: false)
        self.navigationController?.navigationBar.isHidden = true
        hideNavBar()
    }
    
    func hideNavBar() {
        
        navBar.frame.size.height = 64
        navBar.setBackgroundImage(UIImage(), for: .default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupButtons(_ button: UIButton, border: Bool) {
        
        button.layer.cornerRadius = 26
        button.clipsToBounds = true
        
        if border {
            
            button.layer.borderWidth = 2
            button.layer.borderColor = UIColorFromHex(rgbValue: 0xD81B3A).cgColor
        }
    }
    
    @IBAction func driverPressed(_ sender: Any) {
        
        
    }
    
    @IBAction func adminPressed(_ sender: Any) {
        
        
    }
    
    @IBAction func guestPressed(_ sender: Any) {
        
        
    }
    
    
}
