//
//  BlessingTemplate.swift
//  Membo
//
//  Created by Ameya Vichare on 08/10/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import Foundation

class BlessingTemplate {

    var blessingsId: String
    var blessingsTemplate: String
    
    init(blessingsId: String, blessingsTemplate: String) {
        
        self.blessingsId = blessingsId
        self.blessingsTemplate = blessingsTemplate
    }
}
