//
//  profilePicViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 23/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol imageChanged {
    
    func change()
}

class profilePicViewController: UIViewController {

    var delegate: imageChanged?
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var upload: UILabel!
    @IBOutlet weak var mandatoryText: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var guestName: UILabel!
    @IBOutlet weak var pic: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var upConstraint: NSLayoutConstraint!
    var isEdit = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        makePicRound()
        makeNextRound()
        self.navigationController?.navigationBar.isHidden = true
        
        if isEdit {
            
            upload.text = "CHANGE YOUR PROFILE PIC"
            upConstraint.constant = 40.0
            pic.setImage(self.getUserImage(), for: .normal)
            hide(gender)
            hide(guestName)
            hide(mandatoryText)
            nextButton.setTitle("C H A N G E", for: .normal)
        }
        else {
            
            guestName.text = self.getUsername()
            gender.text = self.getGender()
            navBar.isHidden = true
        }
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func hide(_ view: UIView) {
        
        view.isHidden = true
    }
    
    func makePicRound() {
        
        let width = self.view.frame.size.width
        pic.layer.cornerRadius = width * 75/414
        pic.clipsToBounds = true
        pic.layer.borderWidth = 1
        pic.layer.borderColor = UIColor.black.cgColor
    }
    
    func makeNextRound() {
        
        nextButton.layer.cornerRadius = 26
        nextButton.clipsToBounds = true
    }
    
    @IBAction func picPressed(_ sender: Any) {
        
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerControllerSourceType.photoLibrary
        image.mediaTypes = ["public.image"]
        self.present(image, animated: true, completion: nil)
    }
    
    @IBAction func nextPressed(_ sender: Any) {

        var param = [String:Any]()
        
        if pic.imageView?.image != #imageLiteral(resourceName: "profile-placeholder") {
            
            if let base64String = UIImageJPEGRepresentation((pic.imageView?.image)!, 0.7)?.base64EncodedString() {
                
                param = ["userId": self.getUserId(),"platform": "ios", "token": "", "isLogin": 0, "base64String": base64String]
            }
            
            self.postServer(param: param, url: K.MEMBO_ENDPOINT + "updateProfilePicture", completion: { (json) in
                
                print(json)
                
                if json["error"].stringValue == "false" {
                    
                    if self.isEdit {
                        
                        self.Toast(text: "Image changed.")
                        self.delegate?.change()
                        let imageData = UIImagePNGRepresentation((self.pic.imageView?.image!)!)
                        UserDefaults.standard.set(imageData, forKey: "user_image")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                    else {
                        
                        self.onboardAction(json: json)
                    }
                }
            })
        }
        else {
            
            self.Toast(text: "Upload a profile picture to move ahead.")
        }
    }
    
    func onboardAction(json: JSON) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "welcomeVC") as? welcomeViewController {
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        UserDefaults.standard.set(json["user"]["weddingCode"].stringValue, forKey: "wedding_code")
        UserDefaults.standard.set(json["user"]["weddingId"].stringValue, forKey: "wedding_id")
        UserDefaults.standard.set(json["user"]["userId"].stringValue, forKey: "user_id")
        UserDefaults.standard.set(json["user"]["userName"].stringValue, forKey: "user_name")
        UserDefaults.standard.set(json["user"]["apiKey"].stringValue, forKey: "user_api")
        UserDefaults.standard.set(json["user"]["mobileNo"].stringValue, forKey: "user_mobile")
        UserDefaults.standard.set(true, forKey: "isLoggedIn")
        UserDefaults.standard.set(json["user"]["gender"].stringValue, forKey: "user_gender")
        if json["user"]["isAdmin"].stringValue == "0" {
            
            //normal user
            UserDefaults.standard.set(false, forKey: "isAdmin")
        }
        else if json["user"]["isAdmin"].stringValue == "1" {
            
            //admin user
            UserDefaults.standard.set(true, forKey: "isAdmin")
        }
        UserDefaults.standard.set(true, forKey: "isFirstTimeLoginDone")
        
        let imageData = UIImagePNGRepresentation((self.pic.imageView?.image)!)
        UserDefaults.standard.set(imageData, forKey: "user_image")
    }
}

extension profilePicViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            pic.setImage(image, for: .normal)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
