//
//  animatorViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 10/11/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class animatorViewController: UIViewController {
    
    @IBOutlet weak var spinner: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        var imgListArray = [UIImage]()
        for countValue in 1...15
        {
            
            let strImageName : String = "logo-\(countValue)"
            let image  = UIImage(named:strImageName)
            imgListArray.append(image!)
        }

        spinner.animationImages = imgListArray;
        spinner.animationDuration = 1.5
        spinner.startAnimating()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//
//            self.performSegue(withIdentifier: "homeLoad", sender: self)
//        }
    }

}
