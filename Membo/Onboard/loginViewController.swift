//
//  loginViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 22/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import TextFieldEffects
import Firebase

class loginViewController: UIViewController {

    @IBOutlet weak var mobile: HoshiTextField!
    @IBOutlet weak var password: HoshiTextField!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var bottomText: UILabel!
    @IBOutlet weak var countryCodeView: UIView!
    @IBOutlet weak var countryIcon: UIImageView!
    @IBOutlet weak var lab: UILabel!
    @IBOutlet weak var forgot: UIButton!
    @IBOutlet weak var passwordHeight: NSLayoutConstraint!
    
    var forgotPass = Bool()
    var countryName = "India"
    var countryCode = "+91"
    @IBOutlet weak var bottom1: NSLayoutConstraint!
    @IBOutlet weak var bottom2: NSLayoutConstraint!
    @IBOutlet weak var bottom3: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hidePreviousNavBar()
        
        handleKeyBoard()
        
        handleTextfields()
        
        cutomizeLoginButton()
        
        addTapToBottomLabel()
        
        addTap()
        
        setupConstraints()
    }
    
    @IBAction func privacyClicked(_ sender: Any) {
        
        UIApplication.shared.openURL(URL(string: "http://172.104.54.149/wockhosp/privacy_policy.html")!)
    }
    
    func setupConstraints() {
        
        if self.view.frame.size.width == 320.0 {
            
            print("yes")
            
            self.topConstraint.constant = 30
            self.bottom1.constant = 10
            self.bottom2.constant = 10
            self.bottom3.constant = 10
        }
        
        print("\(UIDevice().type)")
        
        if UIDevice().type == .iPadAir2 || UIDevice().type == .iPadAir1 {
            
            self.topConstraint.constant = -40
        }
    }
    
    func addTap() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.countryTapped))
        countryCodeView.addGestureRecognizer(tap)
        
    }
    
    @objc func countryTapped() {
        
        print("tapped")
        
        if let popOverVC = storyboard?.instantiateViewController(withIdentifier: "codeVC") as? countryCodePopViewController {
            self.addChildViewController(popOverVC)
            popOverVC.delegate = self
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            self.didMove(toParentViewController: self)
        }
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        
        if !forgotPass {
            self.passwordHeight.constant = 0
            forgot.setTitle("Back to Login", for: .normal)
            password.isHidden = true
            loginButton.setTitle("CHANGE PASSWORD", for: .normal)
            forgotPass = true
        }
        else {
            self.passwordHeight.constant = 60
            forgotPass = false
            forgot.setTitle("Forgot Password?", for: .normal)
            password.isHidden = false
            loginButton.setTitle("L O G I N", for: .normal)
        }
    }

    func hidePreviousNavBar() {
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func handleKeyBoard() {
        
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let shouldSplash = UserDefaults.standard.value(forKey: "shouldSplash") as? Bool {
            
            if shouldSplash {
                
                self.handleSplash()
                UserDefaults.standard.set(false, forKey: "shouldSplash")
            }
        }
        else {
            
            self.handleSplash()
            UserDefaults.standard.set(false, forKey: "shouldSplash")
        }
    }
    
    func handleTextfields() {
        
        mobile.placeholderColor = UIColorFromHex(rgbValue: 0x999999)
        mobile.textColor = UIColor.black
        mobile.borderInactiveColor = UIColorFromHex(rgbValue: 0x999999)
        mobile.borderActiveColor = UIColorFromHex(rgbValue: 0x999999)
        mobile.placeholderFontScale = 0.9
        
        password.placeholderColor = UIColorFromHex(rgbValue: 0x999999)
        password.textColor = UIColor.black
        password.borderInactiveColor = UIColorFromHex(rgbValue: 0x999999)
        password.borderActiveColor = UIColorFromHex(rgbValue: 0x999999)
        password.placeholderFontScale = 0.9
    }
    
    func cutomizeLoginButton() {
        
        loginButton.layer.cornerRadius = 26
        loginButton.clipsToBounds = true
    }
    
    func addTapToBottomLabel() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleSendToScan(_:)))
        bottomText.addGestureRecognizer(tap)
    }
    
    func handleSendToScan(_ gesture: UIGestureRecognizer) {

        if let vc = storyboard?.instantiateViewController(withIdentifier: "scanVC") as? scanViewController {

            self.navigationController?.pushViewController(vc, animated: true)
        }
        
//        if let vc = storyboard?.instantiateViewController(withIdentifier: "registerVC") as? registerViewController {
//
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        
        if !forgotPass {
            
            if (mobile.text?.characters.count)! > 0 && (password.text?.characters.count)! > 0 {
                
//                if (mobile.text?.characters.count)! >= 10 {
                
                    self.postServer(param: ["mobile": mobile.text!, "password": password.text!], url: K.MEMBO_ENDPOINT + "login", completion: { [unowned self] (json) in
                        
                        print(json)
                        
                        if json["error"].stringValue == "false" {
                            
                            UserDefaults.standard.set(json["user"]["weddingCode"].stringValue, forKey: "wedding_code")
                            UserDefaults.standard.set(json["user"]["weddingId"].stringValue, forKey: "wedding_id")
                            UserDefaults.standard.set(json["user"]["userId"].stringValue, forKey: "user_id")
                            UserDefaults.standard.set(json["user"]["userName"].stringValue, forKey: "user_name")
                            UserDefaults.standard.set(json["user"]["apiKey"].stringValue, forKey: "user_api")
                            UserDefaults.standard.set(json["user"]["mobileNo"].stringValue, forKey: "user_mobile")
                            UserDefaults.standard.set(true, forKey: "isLoggedIn")
                            UserDefaults.standard.set(json["user"]["gender"].stringValue, forKey: "user_gender")
                            UserDefaults.standard.set(json["user"]["rsvpStatus"].stringValue, forKey: "rsvpStatus")
                            UserDefaults.standard.set(json["user"]["profilePic"].stringValue, forKey: "user_image_url")
                            
                            if let onlineUpdated = UserDefaults.standard.value(forKey: "onlineUpdated") as? Bool {
                                
                                if !onlineUpdated {
                                    
                                    UserDefaults.standard.set(true, forKey: "onlineUpdated")
                                    SocketIOManager.sharedInstance.establishConnectionForRealTimeStatus(id: json["user"]["userId"].stringValue)
                                }
                            }
                            
                            let window = UIApplication.shared.keyWindow!
                            
                            if let token = InstanceID.instanceID().token() {
                                
//                                window.makeToast("Uploading Token")
                                print("Uploading token")
                                
                                UserDefaults.standard.set(true, forKey: "tokenUploaded")
                                self.postServer2(param: ["userId": json["user"]["userId"].stringValue, "token": token, "platform": "ios"], url: K.MEMBO_ENDPOINT + "userToken", completion: { (json) in
                                    
                                    print("token: \(token)")
                                    print(json)
                                    
                                    if json["error"].stringValue == "false" {
                                        
                                        
                                    }
                                })
                            }
                            else {
                                
                                UserDefaults.standard.set(false, forKey: "tokenUploaded")
//                                window.makeToast("Token not found.")
                            }
                            
                            //                    SinchCall.sharedInstance.closeConnection()
                            //                    SinchCall.sharedInstance.establishConnection()
                            
                            if json["user"]["isAdmin"].stringValue == "0" {
                                
                                //normal user
                                UserDefaults.standard.set(false, forKey: "isAdmin")
                            }
                            else if json["user"]["isAdmin"].stringValue == "1" {
                                
                                //admin user
                                UserDefaults.standard.set(true, forKey: "isAdmin")
                            }
                            
                            if json["user"]["loginStatus"].stringValue == "1" {
                                
                                //not first time login
                                
                                UserDefaults.standard.set(true, forKey: "isFirstTimeLoginDone")
                                
                                if json["user"]["isAdmin"].stringValue == "0" {
                                    
                                    UserDefaults.standard.set(0, forKey: "admin_identifier")
                                    
                                    let tab = UIStoryboard(name: "TabBar", bundle: nil)
                                    
                                    if let vc = tab.instantiateViewController(withIdentifier: "revealVC") as? SWRevealViewController {
                                        
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                                else if json["user"]["isAdmin"].stringValue == "1" {
                                    
                                    UserDefaults.standard.set(1, forKey: "admin_identifier")
                                    
                                    let homeTab = UIStoryboard(name: "TabBar", bundle: nil)
                                    
                                    if let vc = homeTab.instantiateViewController(withIdentifier: "revealVC") as? SWRevealViewController {
                                        
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }
                                else {

                                    let homeTab = UIStoryboard(name: "TabBar", bundle: nil)
                                    
                                    if let vc = homeTab.instantiateViewController(withIdentifier: "revealLogistics") as? SWRevealViewController {
                                        
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                    
                                    UserDefaults.standard.set(3, forKey: "admin_identifier")
                                }
                                
                                ImageLoader.sharedLoader.imageForUrl(urlString: json["user"]["profilePic"].stringValue, completionHandler: { (image: UIImage?, url: String) in
                                    
                                    let imageData = UIImagePNGRepresentation(image!)
                                    UserDefaults.standard.set(imageData!, forKey: "user_image") //image pic
                                })
                            }
                            else if json["user"]["loginStatus"].stringValue == "0" {
                                
                                //first time login
                                UserDefaults.standard.set(false, forKey: "isFirstTimeLoginDone")
                                
                                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "profilePicVC") as? profilePicViewController {
                                    
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                        else if json["error"].stringValue == "true" {
                            
                            self.Toast(text: json["message"].stringValue)
                        }
                    })
//                }
//                else {
//
//                    self.Toast(text: "Mobile number should atleast be 10 digits long.")
//                }
            }
            else {
                
                self.Toast(text: "Fill in all the details.")
            }
        }
        else {
            
            //api for change password
            
            if (mobile.text?.characters.count)! > 0 {
                
                self.postServer(param: ["mobile": mobile.text!,"cc":countryCode,"cn":countryName], url: K.MEMBO_ENDPOINT + "forgotPassword", completion: { [unowned self] (json) in
                    
                    print(json)
                    
                    if json["error"].stringValue == "false" {
                        
                        self.Toast(text: "New password is sent on your email address.")
                    }
                    else {
                        
                        self.Toast(text: "Something went wrong. Try again later.")
                    }
                })
            }
            else {
                
                self.Toast(text: "Enter your mobile number.")
            }
        }
    }
}

extension loginViewController: countryBackPass {
    
    func passCountry(name: String,code: String, icon: UIImage) {
        
        countryIcon.image = icon
        lab.text = code
        countryName = name
        countryCode = code
    }
}

extension loginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        topConstraint.constant = -40
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if self.view.frame.size.width == 320.0 {
            
            topConstraint.constant = 30
        }
        else if UIDevice().type == .iPadAir2 || UIDevice().type == .iPadAir1 {
            
            self.topConstraint.constant = -40
        }
        else {
            
            topConstraint.constant = 50
        }
    }
}
