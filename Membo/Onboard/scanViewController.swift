//
//  scanViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 23/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit
import QRCodeReader
import AVFoundation

class scanViewController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode], captureDevicePosition: .back)
            $0.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }

    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func initialSetup() {
        
        hidePreviousNavBar()
        loadScanner()
        self.addSpacingToTitle(navBar, title: "SCAN")
    }
    
    func hidePreviousNavBar() {
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func loadScanner() {
        
        guard self.checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
            }
        }
        
        present(readerVC, animated: true, completion: nil)
    }
    
    func checkGuestLimit(guestId: String, weddingId: String) {
        
        self.getServer(url: K.MEMBO_ENDPOINT + "getGuestInviteCount/\(guestId)/\(weddingId)") { [unowned self] (json) in
            
            if json["error"].stringValue == "false" {
                
                if Int(json["inviteCount"].stringValue)! > 0 {
                    
                    // sign up page
                    
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "registerVC") as? registerViewController {
                        
                        vc.friendId = guestId
                        vc.weddingId = weddingId
        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                else {
                    
                    self.Toast(text: "You have no invites left!")
                }
            }
        }
    }
}

extension scanViewController: QRCodeReaderViewControllerDelegate {
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) { [unowned self] in
            
            let qrResult = result.value.components(separatedBy: ",")
            
            if qrResult.count > 0 {
                
                if qrResult.indices.contains(1) {
                    
                    self.checkGuestLimit(guestId: qrResult[1], weddingId: qrResult[0])
                }
            }
            
            let alert = UIAlertController(
                title: "QRCodeReader",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
}
