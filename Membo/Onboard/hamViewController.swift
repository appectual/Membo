//
//  hamViewController.swift
//  Membo
//
//  Created by Ameya Vichare on 24/09/17.
//  Copyright © 2017 appectual. All rights reserved.
//

import UIKit

class hamViewController: UIViewController {

    @IBOutlet weak var hamTable: UITableView!
    
    var current = 0
    
    var hamTitlesAdmin = ["HOME","DASHBOARD","LIVE VIDEO","SCHEDULE","GALLERY","BLESSINGS","PEOPLE TRACKER","FEEDBACK","LOGOUT"]
    
    var hamTitlesGuest = ["HOME","RSVP","MY FACILITIES","GALLERY","GENERATE QR-CODE","BLESSINGS","PEOPLE TRACKER","FEEDBACK","LOGOUT"]
    
    var hamTitlesLog = ["LOGISTIC","LIVE VIDEO","SCHEDULE","PEOPLE TRACKER","FEEDBACK","LOGOUT"]
    
    var hamIconsGuest = [#imageLiteral(resourceName: "homeHam"),#imageLiteral(resourceName: "inviteHam"),#imageLiteral(resourceName: "hotelHam"),#imageLiteral(resourceName: "galleryHam"),#imageLiteral(resourceName: "qrHam"),#imageLiteral(resourceName: "fireworksHam"),#imageLiteral(resourceName: "trackerHam"),#imageLiteral(resourceName: "notepadHam"),#imageLiteral(resourceName: "powerHam")]
    var hamIconsAdmin = [#imageLiteral(resourceName: "homeHam"),#imageLiteral(resourceName: "dashboardHam"),#imageLiteral(resourceName: "liveVid"),#imageLiteral(resourceName: "schedule"),#imageLiteral(resourceName: "galleryHam"),#imageLiteral(resourceName: "fireworksHam"),#imageLiteral(resourceName: "trackerHam"),#imageLiteral(resourceName: "notepadHam"),#imageLiteral(resourceName: "powerHam")]
    var hamIconsLog = [#imageLiteral(resourceName: "homeHam"),#imageLiteral(resourceName: "liveVid"),#imageLiteral(resourceName: "schedule"),#imageLiteral(resourceName: "trackerHam"),#imageLiteral(resourceName: "notepadHam"),#imageLiteral(resourceName: "powerHam")]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hamTable.estimatedRowHeight = 200
        hamTable.rowHeight = UITableViewAutomaticDimension
        hamTable.tableFooterView = UIView(frame: CGRect.zero)
    }
}

extension hamViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 1
        }
        
        if self.isAdmin() == 0 {
            
            return hamTitlesGuest.count
        }
        else if self.isAdmin() == 1 {
            
            return hamTitlesAdmin.count
        }
        else {
            //3
            return hamTitlesLog.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "photo", for: indexPath) as! photoInHamTableViewCell
            addShadow(cell.downLabel)
            brideName(cell.downLabel)
            cell.selectionStyle = .none
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "titles", for: indexPath)
            
            if self.isAdmin() == 0 {
                
                cell.textLabel?.text = hamTitlesGuest[indexPath.row]
                cell.imageView?.image = hamIconsGuest[indexPath.row]
            }
            else if self.isAdmin() == 1 {
                
                cell.textLabel?.text = hamTitlesAdmin[indexPath.row]
                cell.imageView?.image = hamIconsAdmin[indexPath.row]
            }
            else {
                
                cell.textLabel?.text = hamTitlesLog[indexPath.row]
                cell.imageView?.image = hamIconsLog[indexPath.row]
            }
            
            cell.textLabel?.font = UIFont(name: "OpenSans-Regular", size: 13.8)
            cell.textLabel?.textColor = UIColorFromHex(rgbValue: 0x333333)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    
    func brideName(_ lab: UILabel) {

        let attrs1 = [NSFontAttributeName : UIFont(name: "OpenSans-Regular", size: 27.6), NSForegroundColorAttributeName : UIColorFromHex(rgbValue: 0xffffff)]
        
        let attrs2 = [NSFontAttributeName : UIFont(name: "OpenSans-Regular", size: 20.7), NSForegroundColorAttributeName : UIColorFromHex(rgbValue: 0xdc143c)]
        
        let attributedString1 = NSMutableAttributedString(string:"Hardik\n", attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:"weds\n", attributes:attrs2)
        
        let attributedString3 = NSMutableAttributedString(string:"Malvi", attributes:attrs1)
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        lab.attributedText = attributedString1
    }
    
    func addShadow(_ label: UILabel) {
        
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOffset = CGSize(width: 0, height: 0)
        label.layer.shadowOpacity = 1
        label.layer.shadowRadius = 1
    }
}

extension hamViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let revealController: SWRevealViewController? = revealViewController()
        
        switch indexPath.row {
        
        case 0:
            
            if self.isAdmin() == 0 {
                
//                if current == 0 {
//
//                    revealController?.setFrontViewPosition(.left, animated: true)
//                }
//                else {
                
                    //                let tab = UIStoryboard(name: "HomeTab", bundle: nil)
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "tabVC") as? UITabBarController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 0
                    }
//                }
            }
            else if self.isAdmin() == 1 {
                
//                if current == 0 {
//
//                    revealController?.setFrontViewPosition(.left, animated: true)
//                }
//                else {
                
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "tabVC") as? UITabBarController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 0
                    }
//                }
            }
            else if self.isAdmin() == 3 {
                
                if let vc = storyboard?.instantiateViewController(withIdentifier: "revealLogistics") as? SWRevealViewController {
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                    current = 0
                }
            }
        case 1:
            
            if self.isAdmin() == 0 {
                
//                if current == 1 {
//
//                    revealController?.setFrontViewPosition(.left, animated: true)
//                }
//                else {
                
                    let homeTab = UIStoryboard(name: "HomeTab", bundle: nil)
                
                    if let vc = homeTab.instantiateViewController(withIdentifier: "rsvpVC") as? rsvpViewController {
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
//                    current = 1
//                }
            }
            else if self.isAdmin() == 1 {
                
//                if current == 1 {
//
//                    revealController?.setFrontViewPosition(.left, animated: true)
//                }
//                else {

                    if let vc = storyboard?.instantiateViewController(withIdentifier: "revealAdminVC") as? SWRevealViewController {
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
//                    current = 1
//                }
            }
            else if self.isAdmin() == 3 {
                
                let tab = UIStoryboard(name: "liveVideoTab", bundle: nil)
                if let vc = tab.instantiateViewController(withIdentifier: "liveVC") as? liveVideoViewController {
                    
                    revealController?.setFront(vc, animated: true)
                    revealController?.setFrontViewPosition(.left, animated: true)
                    current = 1
                }
            }
        case 2:
            
            if self.isAdmin() == 0 {
                
                if current == 2 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "facilityVC") as? facilitiesViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 2
                    }
                }
            }
            else if self.isAdmin() == 1 {
                
                if current == 2 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    let tab = UIStoryboard(name: "liveVideoTab", bundle: nil)
                    if let vc = tab.instantiateViewController(withIdentifier: "liveVC") as? liveVideoViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 2
                    }
                }
            }
            else if self.isAdmin() == 3 {
                
                let tab = UIStoryboard(name: "ScheduleTab", bundle: nil)
                if let vc = tab.instantiateViewController(withIdentifier: "scheduleVC") as? scheduleViewController {
                    
                    revealController?.setFront(vc, animated: true)
                    revealController?.setFrontViewPosition(.left, animated: true)
                    current = 2
                }
            }

        case 3:
            
            if self.isAdmin() == 0 {
                
                if current == 3 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "albumVC") as? albumViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 3
                    }
                }
            }
            else if self.isAdmin() == 1 {
                
                if current == 3 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    let tab = UIStoryboard(name: "ScheduleTab", bundle: nil)
                    if let vc = tab.instantiateViewController(withIdentifier: "scheduleVC") as? scheduleViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 3
                    }
                }
            }
            else if self.isAdmin() == 3 {
                
                if let vc = storyboard?.instantiateViewController(withIdentifier: "mapVC") as? mapViewController {
                    
                    revealController?.setFront(vc, animated: true)
                    revealController?.setFrontViewPosition(.left, animated: true)
                    current = 3
                }
            }

        case 4:
            
            if self.isAdmin() == 0 {
                
                if current == 4 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "qrVC") as? qrViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 4
                    }
                }
            }
            else if self.isAdmin() == 1 {
                
                if current == 4 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "albumVC") as? albumViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 4
                    }
                }
            }
            else if self.isAdmin() == 3 {
                
                if let vc = storyboard?.instantiateViewController(withIdentifier: "feedVC") as? feedbackViewController {
                    
                    revealController?.setFront(vc, animated: true)
                    revealController?.setFrontViewPosition(.left, animated: true)
                    current = 7
                }
            }
            
        case 5:
            
            if self.isAdmin() == 0 {
                
                if current == 5 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "blessingVC") as? blessingsViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 5
                    }
                }
            }
            else if self.isAdmin() == 1 {
                
                if current == 5 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "blessingVC") as? blessingsViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 5
                    }
                }
            }
            else if self.isAdmin() == 3 {
                
                UserDefaults.standard.set(false, forKey: "isLoggedIn")
                self.navigationController?.popToRootViewController(animated: true)
            }
        
        case 6:
            
            if self.isAdmin() == 0 {
                
                if current == 6 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "mapVC") as? mapViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 6
                    }
                }
            }
            else if self.isAdmin() == 1 {
                
                if current == 6 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "mapVC") as? mapViewController {
                        
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 6
                    }
                }
            }
            
        case 7:
            
            if self.isAdmin() == 0 {
                
                if current == 7 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "feedVC") as? feedbackViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 7
                    }
                }
            }
            else if self.isAdmin() == 1 {
                
                if current == 7 {
                    
                    revealController?.setFrontViewPosition(.left, animated: true)
                }
                else {
                    
                    if let vc = storyboard?.instantiateViewController(withIdentifier: "feedVC") as? feedbackViewController {
                        
                        revealController?.setFront(vc, animated: true)
                        revealController?.setFrontViewPosition(.left, animated: true)
                        current = 7
                    }
                }
            }
        case 8:
            
            self.postServer2(param: ["userId": self.getUserId(), "token": "", "platform": "ios"], url: K.MEMBO_ENDPOINT + "userToken", completion: { (json) in
                
                print(json)

                if json["error"].stringValue == "false" {
                    
                    
                }
            })
            
            UserDefaults.standard.set(false, forKey: "isLoggedIn")
            
            self.navigationController?.popToRootViewController(animated: true)
        default:
            break
        }
    }
}
