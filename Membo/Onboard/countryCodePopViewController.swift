//
//  countryCodePopViewController.swift
//  Lets Partii
//
//  Created by Ameya Vichare on 17/09/17.
//  Copyright © 2017 vit. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol countryBackPass {
    
    func passCountry(name: String,code: String, icon: UIImage)
}

class countryCodePopViewController: UIViewController {
    
    var delegate : countryBackPass?

    @IBOutlet weak var popView: UIView!
    
    @IBOutlet weak var search: UITextField!
    
    var countries = [CountryPick]()
    var filteredCountries = [CountryPick]()
    
    var isSearch = Bool()
    
    @IBOutlet weak var codeTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showAnimation()
        parseJson()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.35)
        popView.layer.cornerRadius = 5
        popView.clipsToBounds = true
        
        codeTable.separatorStyle = .none
        
        search.addTarget(self, action: #selector(self.textFieldChanges(_:)), for: .editingChanged)
    }
    
    func textFieldChanges(_ textfield: UITextField) {
        
        if textfield.text?.characters.count == 0 {
            
            isSearch = false
            
            DispatchQueue.main.async {
                
                self.codeTable.reloadData()
            }
        }
        else {
            
            let searchString = search.text
            
            filteredCountries = countries.filter({ (country) -> Bool in
                let country: NSString = country.name as NSString
                
                return (country.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
            })
            
            DispatchQueue.main.async {
                
                self.codeTable.reloadData()
            }
        }
        
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        
        removeAnimation()
    }
    
    func showAnimation() {
        
        //zoom in animation for popup
        self.view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.2) {
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        
        //zoom out for closing view
        self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        self.view.alpha = 1
        UIView.animate(withDuration: 0.2, animations: {
            self.view.alpha = 0
            self.view.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }) { (result) in
            self.view.removeFromSuperview()
        }
    }

    
    func parseJson() {
        
        
        let frameworkBundle = Bundle(for: type(of: self))
        if let jsonPath = frameworkBundle.path(forResource: "countryCodes", ofType: "json"), let jsonData = try? Data(contentsOf: URL(fileURLWithPath: jsonPath)) {
            
            do {
                if let jsonObjects = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? NSArray {
                    
                    for jsonObject in jsonObjects {
                        
                        let countryObj = jsonObject as! NSDictionary
                        
                        let code = countryObj["code"] as! String
                        let phoneCode = countryObj["dial_code"] as! String
                        let name = countryObj["name"] as! String
                        
                        if let flag = UIImage(named: "\(code.lowercased())") {
                            
                            let country = CountryPick(name: name, code: phoneCode, imageCode: code, image: flag)
                            countries.append(country)
                            DispatchQueue.main.async {
                                
                                self.codeTable.reloadData()
                            }
                        }
                    }
                }
            } catch {
                
                print("error")
            }
        }
    }
}

extension countryCodePopViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        isSearch = true
    }
}

extension countryCodePopViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !isSearch {
            
            return countries.count
        }
        else {
            
            return filteredCountries.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var co: CountryPick
        
        if !isSearch {
            
            co = countries[indexPath.row]
        }
        else {
            
            co = filteredCountries[indexPath.row]
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "country", for: indexPath)
        
        cell.imageView?.image = co.image
        cell.textLabel?.text = co.name
        
        let codeLab = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 22))
        codeLab.text = co.code
        cell.accessoryView = codeLab
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //pass data back
        
        var co: CountryPick
        
        if !isSearch {
            
            co = countries[indexPath.row]
        }
        else {
            
            co = filteredCountries[indexPath.row]
        }
        
        self.delegate?.passCountry(name: co.name, code: co.code, icon: co.image)
        removeAnimation()
    }
}

